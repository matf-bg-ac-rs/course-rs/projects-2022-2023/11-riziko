#include "card.h"
#include "territory.h"

Card::Card()
{

}

Card::Card(int id, Territory *territory)
    : m_id(id),
      m_territory(territory)
{

}

Territory *Card::territory() const {
    return m_territory;
}

CardType Card::cardType() const {
    return m_cardType;
}

std::int32_t Card::id() const {
    return m_id;
}


void Card::setCardType(const CardType &cardType)
{
    m_cardType = cardType;
}

Card::~Card()
{

}



