#ifndef TERRITORYOCCUPYCARD_H
#define TERRITORYOCCUPYCARD_H

#include "cardtask.h"

class Player;

class TerritoryOccupyCard : public CardTask
{
public:
    TerritoryOccupyCard();
    TerritoryOccupyCard(const QString &, std::int32_t numOfTerritories);
    std::int32_t numOfTerritories() const;
    bool checkTask(Player *) const override;
    ~TerritoryOccupyCard(){};

private:
    std::int32_t m_numOfTerritories;
};

#endif // TERRITORYOCCUPYCARD_H
