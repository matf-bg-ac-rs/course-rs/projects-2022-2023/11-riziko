#include "regionoccupycard.h"
#include <iostream>

RegionOccupyCard::RegionOccupyCard()
{

}

RegionOccupyCard::RegionOccupyCard(const QString &path, QVector<Region *> &regions, int32_t numOfRegions):
    CardTask(path),
    m_regions(regions),
    m_numOfRegions(numOfRegions)
{

}

QVector<Region *> RegionOccupyCard::regions() const
{
    return m_regions;
}

std::int32_t RegionOccupyCard::numOfRegions() const
{
    return m_numOfRegions;
}

bool RegionOccupyCard::checkTask(Player *playerToCheck) const
{
    int num = 0;

    for (auto r : m_regions) {
        for (auto t : r->territories()) {
            if (!playerToCheck->territories().contains(t)) {
                return false;
            }
        }
        num += 1;
    }

    if (num == m_numOfRegions) {
        return true;
    }

    return false;
}
