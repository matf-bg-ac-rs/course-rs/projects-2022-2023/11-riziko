#ifndef CARDTASK_H
#define CARDTASK_H

#include <QString>

class Player;

class CardTask
{
public:
    CardTask();
    CardTask(const QString &);

    const QString &path() const;

    virtual bool checkTask(Player *) const = 0;
    virtual ~CardTask() {};

private:
    QString m_path;
};

#endif // CARDTASK_H
