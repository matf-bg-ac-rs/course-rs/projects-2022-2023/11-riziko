#include "tcpserver.h"
#include "ui_tcpserver.h"

#include <QtNetwork>
#include <QPlainTextEdit>

#include <iostream>

TcpServer::TcpServer(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::TcpServer)
    , m_server(new QTcpServer(this))
{
    ui->setupUi(this);

    listenToPort();

    connect(m_server, &QTcpServer::newConnection, this, &TcpServer::newPlayer);

    connect(this, &TcpServer::tryToConnect, this, [this](QTcpSocket* connection){
       QString message = QString("#NEWCONNECTION %1").arg(m_clients.size());
       if (m_clients.size() > 5) {
           message = "#SERVERFULL";
           ui->errormsg->setPlainText("Connection not possible:" + message);
       }

       sendMessage(connection, message);
    });

    connect(this, &TcpServer::printNumberOfPlayers, this, [this](QTcpSocket* connection) {
        if (!lobbyLocked) {
            m_numOfConnectedPlayers++;
            if (m_numOfConnectedPlayers == 5) {
                lobbyLocked = true;
                QString serverSendingMsg = QString("#LOCKLOBBY: All players in the lobby.");
                ui->sendmsg->appendPlainText(serverSendingMsg);
                sendMessage(connection, "#LOCKLOBBY");
            } else if (m_numOfConnectedPlayers < 5){
                QString message = QString("Current number of players is: %1").arg(m_numOfConnectedPlayers);
                ui->sendmsg->appendPlainText(message);
                sendMessage(connection, "#WAITINGFORPLAYERS");
            } else {
                sendMessage(connection, "#DISABLEJOINGAME");
                ui->errormsg->appendPlainText("LOBBYALREADYFULL: No more players can join");
            }
        }
    });

    connect(this, &TcpServer::playerLeftLobby, this, [this]() {
        m_numOfConnectedPlayers--;
        lobbyLocked = false;
        QString message = QString("Current number of players is: %1").arg(m_numOfConnectedPlayers);
        ui->sendmsg->appendPlainText(message);
    });

    connect(this, &TcpServer::playerReady, this, [this](QTcpSocket *connection, const QString &name, const QString &color){

        Color playerColor = Color::RED;
        if (color == "RED")
            playerColor = Color::RED;
        if (color == "GREEN")
            playerColor = Color::GREEN;
        if (color == "BLUE")
            playerColor = Color::BLUE;
        if (color == "YELLOW")
            playerColor = Color::YELLOW;
        if (color == "PURPLE")
            playerColor = Color::PURPLE;

        Player* newPlayer = new Player(name, playerColor, true);
        m_clientToPlayer[connection] = newPlayer;
        m_playerToClient[newPlayer] = connection;
        m_players.push_back(newPlayer);
        m_numOfReadyPlayers++;
        ui->sendmsg->appendPlainText("#WAITINGFORALL " + QString("%1").arg(m_numOfReadyPlayers));


        if (m_numOfReadyPlayers == 5) {
            foreach (QTcpSocket* client, m_clients) {
                if (gameInitiated == false) {
                    sendMessage(client, "#ALLPLAYERSREADY false");
                    gameInitiated = true;
                }
                else
                    sendMessage(client, "#ALLPLAYERSREADY true");
            }
        }
    });

    connect(this, &TcpServer::instanceLoop, this, [this] (QTcpSocket* connection) {
        m_playerToPlayer[m_players[0]] = m_players[1];
        m_playerToPlayer[m_players[1]] = m_players[2];
        m_playerToPlayer[m_players[2]] = m_players[3];
        m_playerToPlayer[m_players[3]] = m_players[4];
        m_playerToPlayer[m_players[4]] = m_players[0];
        if (connection == m_clients[1]) {
            sendMessage(connection, "#BREAKSIGNAL");
        }
        else if (connection == m_clients[2]) {
            sendMessage(connection, "#BREAKSIGNAL");
        }
        else if (connection == m_clients[3]) {
            sendMessage(connection, "#BREAKSIGNAL");
        }
        else if (connection == m_clients[4]) {
            sendMessage(connection, "#BREAKSIGNAL");
        }
        if (connection == m_clients[0]) {
            if (!alreadyInLoop) {
                alreadyInLoop = true;
                sendMessage(connection, "#STARTLOOP");
            } else {
                sendMessage(connection, "#BREAKSIGNAL");
            }
        }
    });

    connect(this, &TcpServer::removePlayer, this, [this] (QTcpSocket* connection) {
        if (m_numOfConnectedPlayers > 0) {
            m_numOfConnectedPlayers--;
        }
        QString message = QString("Current number of players is: %1").arg(m_numOfConnectedPlayers);
        ui->sendmsg->appendPlainText(message);
        sendMessage(connection, "#CLOSECLIENT");
    });

    connect(this, &TcpServer::startInitiated, this, [this] () {
        if (!initializedGame) {
            initializedGame = true;
            Initializer* init = new Initializer();
            init->initializerTerritory(m_players);
            init->initializeRegions();
            init->initializeCardTasks(m_players);
            init->initializeCards();
//            game->setPlayers(m_players);
//            game->setTerritories(init->initializerTerritory(m_players));
//            game->setRegions(init->initializeRegions());
//            game->setCards(init->initializeCards());
//            init->initializeCardTasks(m_players);
        }
        ui->sendmsg->appendPlainText("#GAMEINITIALIZED");
        ui->sendmsg->appendPlainText("#UPDATESTARTED");
        foreach (auto client, m_clients) {
            QString initGuiUpdate = "";
            foreach(auto clientPlayer, m_clientToPlayer) {
                Color color = clientPlayer->color();
                QString playerColor;
                if (color == Color::RED)
                    playerColor = "RED";
                if (color == Color::GREEN)
                    playerColor = "GREEN";
                if (color == Color::BLUE)
                    playerColor = "BLUE";
                if (color == Color::YELLOW)
                    playerColor = "YELLOW";
                if (color == Color::PURPLE)
                    playerColor = "PURPLE";
                QString name = clientPlayer->name();
                initGuiUpdate += name + " " + playerColor;
                for (auto territory : clientPlayer->territories()) {
                    QString territoryName = territory->name();
                    initGuiUpdate += " " + territoryName;
                }
                initGuiUpdate += " ";
            }
            sendMessage(client, "#INITUPDATE " + initGuiUpdate + " " + m_clientToPlayer[client]->cardTask()->path());
        }
    });

    connect(this, &TcpServer::taskColorAndInfo, this, [this] (QTcpSocket* connection) {
           Color tempColor = m_clientToPlayer[connection]->color();
           QString curPlayerColor;
           if (tempColor == Color::RED)
               curPlayerColor = "RED";
           if (tempColor == Color::GREEN)
               curPlayerColor = "GREEN";
           if (tempColor == Color::BLUE)
               curPlayerColor = "BLUE";
           if (tempColor == Color::YELLOW)
               curPlayerColor = "YELLOW";
           if (tempColor == Color::PURPLE)
               curPlayerColor = "PURPLE";
           sendMessage(connection, "#TASKCOLORINFO " + m_clientToPlayer[connection]->name() + " " + curPlayerColor);
    });

    connect(this, &TcpServer::requestEnable, this, [this] (QTcpSocket* connection) {
        auto territories = m_clientToPlayer[connection]->territories();
        QString terList;
        for (auto territory : territories) {
            terList += territory->name() + " ";
        }
        sendMessage(connection, "#REQUESTAPPROVED " + terList);
    });

    connect(this, &TcpServer::updateEveryoneWaiting, this, [this] (QTcpSocket* connection, QString updateContent) {
        for (auto playerToClient : m_playerToClient) {
            if (playerToClient != connection) {
                sendMessage(playerToClient, "#WAITINGUPDATE " + updateContent);
            }
        }
    });

    connect(this, &TcpServer::nextPlayer, this, [this] (QTcpSocket* connection) {
       auto c2p = m_clientToPlayer[connection];
       auto p2p = m_playerToPlayer[c2p];
       auto p2c = m_playerToClient[p2p];
       sendMessage(p2c, "#STARTLOOP");
    });

    connect(this, &TcpServer::requestNeighbours, this, [this] (QTcpSocket* connection, QString currentTerritory) {
        auto player = m_clientToPlayer[connection];

        QString cbNeighbour = "";
        for(auto territory : player->territories()) {
            if (territory->name() == currentTerritory)
                for(auto neighbour : territory->neighborTerritories()) {
                        if(neighbour->owner()->name() != player->name()) {
                                cbNeighbour += neighbour->name() + " ";
                        }
                }
        }
        cbNeighbour.remove(cbNeighbour.length() - 1, 1);

        sendMessage(connection, "#NEIGHBOURUPDATE " + cbNeighbour);
    });

    connect(this, &TcpServer::throughDices, this, [this] (QTcpSocket* connection, QString attackTerritoryName, QString attackTerritoryTanks, QString defenceTerritoryName, QString defenceTerritoryTanks) {
        Territory *attackTerritory;
        Territory *defenceTerritory;

        for (auto territory : m_clientToPlayer[connection]->territories()) {
            if (territory->name() == attackTerritoryName) {
                attackTerritory = territory;
                break;
            }
        }

        for (auto territory : attackTerritory->neighborTerritories()) {
            if (territory->name() == defenceTerritoryName) {
                defenceTerritory = territory;
                break;
            }
        }

        std::int32_t numTanksAtt = attackTerritoryTanks.toInt();
        std::int32_t numTanksDef = defenceTerritoryTanks.toInt();
        std::cout << numTanksAtt << " " << numTanksDef << std::endl;

        int attDice1 = QRandomGenerator::global()->bounded(1,7);
        int attDice2 = QRandomGenerator::global()->bounded(1,7);
        int attDice3 = QRandomGenerator::global()->bounded(1,7);

        int defDice1 = QRandomGenerator::global()->bounded(1,7);
        int defDice2 = QRandomGenerator::global()->bounded(1,7);
        int defDice3 = QRandomGenerator::global()->bounded(1,7);

        if(numTanksAtt == 2) {
            attDice2 = 0;
            attDice3 = 0;
        }
        if(numTanksAtt == 3) {
            attDice3 = 0;
        }

        if(numTanksDef == 1) {
            defDice2 = 0;
            defDice3 = 0;
        }

        if(numTanksDef == 2) {
            defDice3 = 0;
        }

        std::vector<std::int32_t> attDices({attDice1,attDice2,attDice3});
        sort(attDices.begin(), attDices.end());
        std::vector<std::int32_t> defDices({defDice1,defDice2,defDice3});
        sort(defDices.begin(), defDices.end());

        QVector<int> dices({attDices[2], attDices[1], attDices[0], defDices[2], defDices[1], defDices[0]});
        QString throwDiceMsg = "";

        if(attDices[2] > defDices[2]) {
            numTanksDef--;
        } else {
            numTanksAtt--;
        }

        if(attDices[1] != 0 && defDices[1] != 0) {
            if(attDices[1] > defDices[1]) {
                numTanksDef--;
            } else {
                numTanksAtt--;
            }
        }

        if(attDices[0] != 0 && defDices[0] != 0) {
            if(attDices[0] > defDices[0]) {
                numTanksDef--;
            } else {
                numTanksAtt--;
            }
        }


        if(numTanksDef <= 0) {
            throwDiceMsg += attackTerritoryName + " " + QString::number(numTanksAtt - 1) + " " + defenceTerritoryName + " " + QString::number(1) + " ";

            std::cout << numTanksAtt << " " << numTanksDef << std::endl;

            for (auto dice : dices) {
                throwDiceMsg += QString::number(dice) + " ";
            }

            throwDiceMsg.remove(throwDiceMsg.length() - 1, 1);
            sendMessage(connection, "#TERRITORYWON " + throwDiceMsg);
        } else {

            throwDiceMsg += attackTerritoryName + " " + QString::number(numTanksAtt) + " " + defenceTerritoryName + " " + QString::number(numTanksDef) + " ";

            std::cout << numTanksAtt << " " << numTanksDef << std::endl;

            for (auto dice : dices) {
                throwDiceMsg += QString::number(dice) + " ";
            }

            throwDiceMsg.remove(throwDiceMsg.length() - 1, 1);
            std::cout << throwDiceMsg.toStdString() << std::endl;
            sendMessage(connection, "#DRAWTHROWUPDATE " + throwDiceMsg);
        }
    });

    connect(this, &TcpServer::attackUpdate, this, [this] (QTcpSocket* connection, QString atkName, QString atkTanks, QString defName, QString defTanks) {
        for (auto playerToClient : m_playerToClient) {
            if (playerToClient != connection) {
                sendMessage(playerToClient, "#ATTACKUPDATE " + atkName + " " + atkTanks + " " + defName + " " + defTanks);
            }
        }
    });

    connect(this, &TcpServer::attackWon, this, [this] (QTcpSocket* connection, QString atkName, QString atkTanks, QString newTer, QString newTanks, QString newColor) {
        for (auto playerToClient : m_playerToClient) {
            if (playerToClient != connection) {
                sendMessage(playerToClient, "#ATTACKWON " + atkName + " " + atkTanks + " " + newTer + " " + newTanks + " " + newColor);
            }
        }
    });

    printIpAddressAndPort();
}

TcpServer::~TcpServer()
{
    delete ui;
}

void TcpServer::disconnectButtonHandler(QList<QTcpSocket*> m_clients)
{
    ui->disconnect_server->setEnabled(!m_clients.isEmpty());
}

void TcpServer::printIpAddressAndPort()
{
    ui->ipaddress->setText(m_server->serverAddress().toString());
    ui->port->setText(QString::number(m_server->serverPort()));
}

void TcpServer::listenToPort()
{
    if (!m_server->listen(QHostAddress::LocalHost, 23623)) {
        ui->errormsg->setPlainText("Connection to server failed:" + m_server->errorString());
        return;
    }
}

void TcpServer::newPlayer()
{
    while (m_server->hasPendingConnections()) {
        QTcpSocket* connection = m_server->nextPendingConnection();

        m_clients << connection;
        disconnectButtonHandler(m_clients);

        connect(connection, &QTcpSocket::disconnected, this, &TcpServer::playerDisconnected);
        connect(connection, &QTcpSocket::readyRead, this, &TcpServer::messageReceived);

        ui->receivemsg->insertPlainText(QString("New Player connected by ip: %1\nThrough port is: %2\n").arg(connection->peerAddress().toString(),
                                                                                                            QString::number(connection->peerPort())));
    }
}

void TcpServer::playerDisconnected()
{
    QTcpSocket* connection = qobject_cast<QTcpSocket*>(sender());
    if (connection) {
        ui->receivemsg->insertPlainText(QString("Disconnected player from server with ip address: %1\nServer port is: %2\n").arg(connection->peerAddress().toString(),
                                                                                                                                 QString::number(connection->peerPort())));

        m_clients.removeOne(connection);
        connection->deleteLater();

        disconnectButtonHandler(m_clients);
    }
}

void TcpServer::messageReceived()
{
    QTcpSocket* connection = qobject_cast<QTcpSocket*>(sender());
    if (connection) {
        QByteArray buffer;

        QDataStream socketStream(connection);
        socketStream.setVersion(QDataStream::Qt_5_6);

        socketStream.startTransaction();
        socketStream >> buffer;

        if (!socketStream.commitTransaction()) {
            ui->receivemsg->appendPlainText(QString("%1").arg(connection->socketDescriptor()));
            return;
        }

        QString message = QString("%1").arg(QString::fromStdString(buffer.toStdString()));
        ui->receivemsg->insertPlainText("Sending message: " + message + "\n" );
        parseMessage(message, connection);
    }
}

void TcpServer::on_disconnect_server_clicked()
{
    foreach (QTcpSocket *socket, m_clients) {
        socket->close();
    }
    ui->disconnect_server->setEnabled(false);
}

void TcpServer::sendMessage(QTcpSocket *connection, const QString &message)
{
    QByteArray data;
    QDataStream stream(&data, QIODevice::WriteOnly);

    stream << (quint32)0;
    stream << message.toUtf8();
    stream.device()->seek(0);
    stream << (quint32)data.size();

    connection->write(data);
}

void TcpServer::parseMessage(QString message, QTcpSocket* connection)
{
    auto messageContent = message.split(' ');
    auto clientMessage = messageContent[0];
    if(clientMessage == "#CONNECT"){
        emit tryToConnect(connection);
    }
    else if (clientMessage == "#NEWPLAYER") {
        emit printNumberOfPlayers(connection);
    }
    else if (clientMessage == "#PLAYERLEFTLOBBY") {
        emit playerLeftLobby(connection);
    }
    else if (clientMessage == "#PLAYERREADY") {
        auto name = messageContent[1];
        auto color = messageContent[2];
        emit playerReady(connection, name, color);
    }
    else if (clientMessage == "#REMOVEPLAYER") {
        emit removePlayer(connection);
    }
    else if (clientMessage == "#INITIATESTART") {
        ui->receivemsg->appendPlainText("#INITIATEDGAME: All player initiated");
        emit startInitiated(connection);
    }
    else if (clientMessage == "#UPDATEGAMEUI") {
        emit updateGameUi(connection);
    }
    else if (clientMessage == "#TASKCOLORANDINFO") {
        emit taskColorAndInfo(connection);
    }
    else if (clientMessage == "#REQUESTENABLE") {
        emit requestEnable(connection);
    }
    else if (clientMessage == "#INSTANCELOOP") {
        emit instanceLoop(connection);
    }
    else if (clientMessage == "#TANUPDATES") {
        QString updateContent = "";
        for (int i = 1 ; i <= ((messageContent.size() - 1)) / 2; i++) {
            updateContent += messageContent[i] + " " + messageContent[i+7] + " ";

        }
        updateContent.remove(updateContent.length() - 1, 1);
        emit updateEveryoneWaiting(connection, updateContent);
    }
    else if (clientMessage == "#NEXTPLAYER") {
        emit nextPlayer(connection);
    }
    else if (clientMessage == "#REQUESTNEIGHBOURS") {
        emit requestNeighbours(connection, messageContent[1]);
    }
    else if (clientMessage == "#THROUGHDICES") {
        emit throughDices(connection, messageContent[1], messageContent[2], messageContent[3], messageContent[4]);
    }
    else if (clientMessage == "#ATTACKUPDATE") {
        emit attackUpdate(connection, messageContent[1], messageContent[2], messageContent[3], messageContent[4]);
    }
    else if (clientMessage == "#ATTACKWON") {
        emit attackWon(connection, messageContent[1], messageContent[2], messageContent[3], messageContent[4], messageContent[5]);
    }
}

void TcpServer::errorHandler(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
        case QAbstractSocket::RemoteHostClosedError:
        break;
        case QAbstractSocket::HostNotFoundError:
            QMessageBox::information(this, "QTCPServer", "The host was not found. Please check the host name and port settings.");
        break;
        case QAbstractSocket::ConnectionRefusedError:
            QMessageBox::information(this, "QTCPServer", "The connection was refused by the peer. Make sure QTCPServer is running, and check that the host name and port settings are correct.");
        break;
        default:
            QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());
            QMessageBox::information(this, "QTCPServer", QString("The following error occurred: %1.").arg(socket->errorString()));
        break;
    }
}


