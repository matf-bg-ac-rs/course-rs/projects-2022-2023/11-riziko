#include "settings.h"
#include "player.h"

std::int32_t Settings::numOfPlayers() const
{
    return m_numOfPlayers;
}

std::int32_t Settings::numOfStartingTanks() const
{
    return m_numOfStartingTanks;
}

Goal Settings::goal() const
{
    return m_goal;
}

const QVector<Player *> &Settings::players() const
{
    return m_players;
}

Settings::Settings()
{

}

Settings::Settings(int32_t numOfPlayers, int32_t numOfStartingTanks, Goal goal, const QVector<Player *> &players) : m_numOfPlayers(std::move(numOfPlayers)),
    m_numOfStartingTanks(std::move(numOfStartingTanks)),
    m_goal(goal),
    m_players(players)
{}
