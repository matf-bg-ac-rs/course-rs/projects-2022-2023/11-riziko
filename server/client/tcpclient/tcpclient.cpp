#include "tcpclient.h"
#include "ui_tcpclient.h"
#include "ui_mainwindow.h"

#include <QWidget>
#include <QtNetwork>
#include <QMessageBox>

TcpClient::TcpClient(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::TcpClient)
    , m_socket(new QTcpSocket(this))
{
    ui->setupUi(this);

    ui->ipaddress->setText(QHostAddress(QHostAddress::LocalHost).toString());
    ui->port->setValue(23623);

    m_mainWindow = new MainWindow(this);
    m_mainWindow->setAttribute(Qt::WA_DeleteOnClose);

    connect(m_mainWindow, &MainWindow::closeGame, this, [this] (){
        sendMessage("#REMOVEPLAYER");
    });
    connect(this, &TcpClient::closeClient, this, [this] {
        this->close();
    });

    connect(m_socket, &QTcpSocket::connected, this, &TcpClient::connectedToServer);
    connect(m_socket, &QTcpSocket::disconnected, this, &TcpClient::disconnectedFromServer);
    connect(m_socket, &QTcpSocket::readyRead, this, &TcpClient::readMessage);

    connect(this, &TcpClient::connection, this, &TcpClient::sendMessage);

    connect(m_mainWindow->getUi()->Albania, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Albania";
        QString number = m_mainWindow->getUi()->Albania->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Albania->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);

            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });
    connect(m_mainWindow->getUi()->Austria, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Austria";
        QString number = m_mainWindow->getUi()->Austria->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Austria->setText(newNumber);


        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Belarus, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Belarus";
        QString number = m_mainWindow->getUi()->Belarus->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Belarus->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }
    });

    connect(m_mainWindow->getUi()->Belgium, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Belgium";
        QString number = m_mainWindow->getUi()->Belgium->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Belgium->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }
    });

    connect(m_mainWindow->getUi()->Bosnia, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Bosnia";
        QString number = m_mainWindow->getUi()->Bosnia->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Bosnia->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }
    });

    connect(m_mainWindow->getUi()->Bulgaria, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Bulgaria";
        QString number = m_mainWindow->getUi()->Bulgaria->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Bulgaria->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }
    });

    connect(m_mainWindow->getUi()->Croatia, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Croatia";
        QString number = m_mainWindow->getUi()->Croatia->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Croatia->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Czech, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Czech";
        QString number = m_mainWindow->getUi()->Czech->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Czech->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }
    });

    connect(m_mainWindow->getUi()->Denmark, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Denmark";
        QString number = m_mainWindow->getUi()->Denmark->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Denmark->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Estonia, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Estonia";
        QString number = m_mainWindow->getUi()->Estonia->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Estonia->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Finland, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Finland";
        QString number = m_mainWindow->getUi()->Finland->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Finland->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->France, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "France";
        QString number = m_mainWindow->getUi()->France->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->France->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Germany, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Germany";
        QString number = m_mainWindow->getUi()->Germany->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Germany->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }
    });

    connect(m_mainWindow->getUi()->Greece, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Greece";
        QString number = m_mainWindow->getUi()->Greece->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Greece->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }
    });

    connect(m_mainWindow->getUi()->Hungary, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Hungary";
        QString number = m_mainWindow->getUi()->Hungary->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Hungary->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Iceland, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Iceland";
        QString number = m_mainWindow->getUi()->Iceland->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Iceland->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }
    });

    connect(m_mainWindow->getUi()->Ireland, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Ireland";
        QString number = m_mainWindow->getUi()->Ireland->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Ireland->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Italy, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Italy";
        QString number = m_mainWindow->getUi()->Italy->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Italy->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }
    });

    connect(m_mainWindow->getUi()->Latvia, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Latvia";
        QString number = m_mainWindow->getUi()->Latvia->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Latvia->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }
    });

    connect(m_mainWindow->getUi()->Lithuania, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Lithuania";
        QString number = m_mainWindow->getUi()->Lithuania->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Lithuania->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Macedonia, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Macedonia";
        QString number = m_mainWindow->getUi()->Macedonia->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Macedonia->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Moldova, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Moldova";
        QString number = m_mainWindow->getUi()->Moldova->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Moldova->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Netherlands, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Netherlands";
        QString number = m_mainWindow->getUi()->Netherlands->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Netherlands->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Norway, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Norway";
        QString number = m_mainWindow->getUi()->Norway->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Norway->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Poland, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Poland";
        QString number = m_mainWindow->getUi()->Poland->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Poland->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Portugal, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Portugal";
        QString number = m_mainWindow->getUi()->Portugal->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Portugal->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Romania, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Romania";
        QString number = m_mainWindow->getUi()->Romania->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Romania->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Serbia, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Serbia";
        QString number = m_mainWindow->getUi()->Serbia->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Serbia->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Slovakia, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Slovakia";
        QString number = m_mainWindow->getUi()->Slovakia->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Slovakia->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Slovenia, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Slovenia";
        QString number = m_mainWindow->getUi()->Slovenia->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Slovenia->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }
    });

    connect(m_mainWindow->getUi()->Spain, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Spain";
        QString number = m_mainWindow->getUi()->Spain->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Spain->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }
    });

    connect(m_mainWindow->getUi()->Sweden, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Sweden";
        QString number = m_mainWindow->getUi()->Sweden->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Sweden->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }
    });

    connect(m_mainWindow->getUi()->Switzerland, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Switzerland";
        QString number = m_mainWindow->getUi()->Switzerland->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Switzerland->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });

    connect(m_mainWindow->getUi()->Ukraine, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "Ukraine";
        QString number = m_mainWindow->getUi()->Ukraine->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->Ukraine->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }
    });

    connect(m_mainWindow->getUi()->UnitedKingdom, &QPushButton::clicked, this, [this] {
        numOfTanks--;
        QString territoryName = "UnitedKingdom";
        QString number = m_mainWindow->getUi()->UnitedKingdom->text();
        int32_t territoryNumber = number.toInt();
        territoryNumber++;
        QString newNumber = QString::number(territoryNumber);
        m_mainWindow->getUi()->UnitedKingdom->setText(newNumber);

        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            QString currentNumberOfTanks = "";
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);
                currentNumberOfTanks += button->text() + " ";
                if(button) {
                     button->setDisabled(true);
                }
            }
            currentNumberOfTanks.remove(currentNumberOfTanks.length() - 1, 1);

            sendMessage("#TANUPDATES " + currentTerritories + " " + currentNumberOfTanks);
            if (playerMove) {
                m_mainWindow->getUi()->pbPlayMove->setEnabled(true);
            }
            m_mainWindow->getUi()->pbNextPlayer->setEnabled(true);
            currentTanksInOrder = currentNumberOfTanks;
        }

    });


    connect(this, &TcpClient::startMenu, this, [this]() {
        this->hide();
        m_mainWindow->show();
        m_mainWindow->getUi()->pbAddPlayer->setText("Ready");
        m_mainWindow->getUi()->pbStart->setDisabled(true);
        m_mainWindow->getUi()->pbPlayer1->setDisabled(true);
        m_mainWindow->getUi()->pbPlayer2->setDisabled(true);
        m_mainWindow->getUi()->pbPlayer3->setDisabled(true);
        m_mainWindow->getUi()->pbPlayer4->setDisabled(true);
        m_mainWindow->getUi()->pbPlayer5->setDisabled(true);
        m_mainWindow->getUi()->pbAttack->setDisabled(true);
        m_mainWindow->getUi()->pbCards->setDisabled(true);
        m_mainWindow->getUi()->pbExcangeCards->setDisabled(true);
        m_mainWindow->getUi()->pbDrawCard->setDisabled(true);
        m_mainWindow->getUi()->pbNextPlayer->setDisabled(true);
        m_mainWindow->getUi()->wAttack->hide();
        m_mainWindow->getUi()->pbAttack->setDisabled(true);
        // DISABLE TERRITORIES
        m_mainWindow->getUi()->Albania->setDisabled(true);
        m_mainWindow->getUi()->Austria->setDisabled(true);
        m_mainWindow->getUi()->Belarus->setDisabled(true);
        m_mainWindow->getUi()->Belgium->setDisabled(true);
        m_mainWindow->getUi()->Bosnia->setDisabled(true);
        m_mainWindow->getUi()->Bulgaria->setDisabled(true);
        m_mainWindow->getUi()->Croatia->setDisabled(true);
        m_mainWindow->getUi()->Czech->setDisabled(true);
        m_mainWindow->getUi()->Denmark->setDisabled(true);
        m_mainWindow->getUi()->Estonia->setDisabled(true);
        m_mainWindow->getUi()->Finland->setDisabled(true);
        m_mainWindow->getUi()->France->setDisabled(true);
        m_mainWindow->getUi()->Germany->setDisabled(true);
        m_mainWindow->getUi()->Greece->setDisabled(true);
        m_mainWindow->getUi()->Hungary->setDisabled(true);
        m_mainWindow->getUi()->Iceland->setDisabled(true);
        m_mainWindow->getUi()->Ireland->setDisabled(true);
        m_mainWindow->getUi()->Italy->setDisabled(true);
        m_mainWindow->getUi()->Latvia->setDisabled(true);
        m_mainWindow->getUi()->Lithuania->setDisabled(true);
        m_mainWindow->getUi()->Macedonia->setDisabled(true);
        m_mainWindow->getUi()->Moldova->setDisabled(true);
        m_mainWindow->getUi()->Netherlands->setDisabled(true);
        m_mainWindow->getUi()->Norway->setDisabled(true);
        m_mainWindow->getUi()->Poland->setDisabled(true);
        m_mainWindow->getUi()->Portugal->setDisabled(true);
        m_mainWindow->getUi()->Romania->setDisabled(true);
        m_mainWindow->getUi()->Serbia->setDisabled(true);
        m_mainWindow->getUi()->Slovakia->setDisabled(true);
        m_mainWindow->getUi()->Slovenia->setDisabled(true);
        m_mainWindow->getUi()->Spain->setDisabled(true);
        m_mainWindow->getUi()->Sweden->setDisabled(true);
        m_mainWindow->getUi()->Switzerland->setDisabled(true);
        m_mainWindow->getUi()->Ukraine->setDisabled(true);
        m_mainWindow->getUi()->UnitedKingdom->setDisabled(true);
    });

    connect(m_mainWindow->getUi()->pbJoinGame, &QPushButton::clicked, this, [this] () {
       sendMessage("#NEWPLAYER");
       m_mainWindow->getUi()->leName->setText("");
       m_mainWindow->getUi()->cbColor->setCurrentIndex(0);
    });

    connect(m_mainWindow->getUi()->pbBack, &QPushButton::clicked, this, [this] () {
       sendMessage("#PLAYERLEFTLOBBY");
    });

    connect(this, &TcpClient::lockLobby, this, [this] () {
        m_mainWindow->getUi()->pbBack->setEnabled(false);
    });

    connect(this, &TcpClient::disableJoin, this, [this] () {
        m_mainWindow->getUi()->pbJoinGame->setEnabled(false);
    });

    connect(m_mainWindow->getUi()->pbAddPlayer, &QPushButton::clicked, this, [this] (){
       auto name = m_mainWindow->getUi()->leName->text();
       auto color = m_mainWindow->getUi()->cbColor->currentText();
       QString index = QString::number(m_mainWindow->getUi()->cbColor->currentIndex());
       m_mainWindow->getUi()->leName->setDisabled(true);
       m_mainWindow->getUi()->cbColor->setDisabled(true);
       m_mainWindow->getUi()->pbAddPlayer->setDisabled(true);
       m_mainWindow->getUi()->pbBack->setDisabled(true);
       sendMessage("#PLAYERREADY " + name + " " + color + " " + index);
    });

    connect(this, &TcpClient::removeColor, this, [this] (QString index) {
       m_mainWindow->getUi()->cbColor->removeItem(index.toInt());
    });

    connect(this, &TcpClient::allPlayersReady, this, [this] (bool gameInitiated) {
        if (!gameInitiated) {
            sendMessage("#INITIATESTART");
        }
        m_mainWindow->getUi()->stackedWidget->setCurrentIndex(2);
    });

    connect(this, &TcpClient::initUpdate, this, [this] (QString info) {
        auto infoContent = info.split(' ');
        for (i = 0 ; i < 5 ; i++) {
            if (i == 0) {
                m_mainWindow->getUi()->pbPlayer1->setText(infoContent[i*9]);
                m_mainWindow->getUi()->pbPlayer1->setStyleSheet({"background-color: " + infoContent[i*9+1].toLower()});
                for (int j = 2 ; j < 9 ; j++) {
                    QPushButton* button = this->findChild<QPushButton*>(infoContent[i*9+j]);

                    if(button) {
                        button->setStyleSheet({"background-color: " + infoContent[i*9+1]});
                        button->setText(QString::number(1));
                    }
                }
            }
            else if (i == 1) {
                m_mainWindow->getUi()->pbPlayer2->setText(infoContent[i*9]);
                m_mainWindow->getUi()->pbPlayer2->setStyleSheet({"background-color: " + infoContent[i*9+1].toLower()});
                for (int j = 2 ; j < 9 ; j++) {
                    QPushButton* button = this->findChild<QPushButton*>(infoContent[i*9+j]);

                    if(button) {
                        button->setStyleSheet({"background-color: " + infoContent[i*9+1]});
                        button->setText(QString::number(1));
                    }
                }
            }
            else if (i == 2) {
                m_mainWindow->getUi()->pbPlayer3->setText(infoContent[i*9]);
                m_mainWindow->getUi()->pbPlayer3->setStyleSheet({"background-color: " + infoContent[i*9+1].toLower()});
                for (int j = 2 ; j < 9 ; j++) {
                    QPushButton* button = this->findChild<QPushButton*>(infoContent[i*9+j]);

                    if(button) {
                        button->setStyleSheet({"background-color: " + infoContent[i*9+1]});
                        button->setText(QString::number(1));
                    }
                }
            }
            else if (i == 3) {
                m_mainWindow->getUi()->pbPlayer4->setText(infoContent[i*9]);
                m_mainWindow->getUi()->pbPlayer4->setStyleSheet({"background-color: " + infoContent[i*9+1].toLower()});
                for (int j = 2 ; j < 9 ; j++) {
                    QPushButton* button = this->findChild<QPushButton*>(infoContent[i*9+j]);

                    if(button) {
                        button->setStyleSheet({"background-color: " + infoContent[i*9+1]});
                        button->setText(QString::number(1));
                    }
                }
            }
            else if (i == 4) {
                m_mainWindow->getUi()->pbPlayer5->setText(infoContent[i*9]);
                m_mainWindow->getUi()->pbPlayer5->setStyleSheet({"background-color: " + infoContent[i*9+1].toLower()});
                for (int j = 2 ; j < 9 ; j++) {
                    QPushButton* button = this->findChild<QPushButton*>(infoContent[i*9+j]);

                    if(button) {
                        button->setStyleSheet({"background-color: " + infoContent[i*9+1]});
                        button->setText(QString::number(1));
                    }
                }
            }
        }
        taskCardPath = infoContent[infoContent.size()-1];
        sendMessage("#TASKCOLORANDINFO");
    });

    connect(this, &TcpClient::taskCardColor, this, [this] (QString color) {
        if (m_mainWindow->getUi()->pbPlayer1->text() == m_player->name()) {
            playerButton = m_mainWindow->getUi()->pbPlayer1;
        }
        else if (m_mainWindow->getUi()->pbPlayer2->text() == m_player->name()) {
            playerButton = m_mainWindow->getUi()->pbPlayer2;
        }
        else if (m_mainWindow->getUi()->pbPlayer3->text() == m_player->name()) {
            playerButton = m_mainWindow->getUi()->pbPlayer3;
        }
        else if (m_mainWindow->getUi()->pbPlayer4->text() == m_player->name()) {
            playerButton = m_mainWindow->getUi()->pbPlayer4;
        }
        else if (m_mainWindow->getUi()->pbPlayer5->text() == m_player->name()) {
            playerButton = m_mainWindow->getUi()->pbPlayer5;
        }

        QPixmap pix(taskCardPath);
        m_mainWindow->getUi()->lTaskCard->setPixmap(pix.scaled(171, 171, Qt::KeepAspectRatio));
        m_mainWindow->getUi()->leTaskCardText->setStyleSheet({"background-color: " + color.toLower()});

        sendMessage("#REQUESTENABLE");
    });

    connect(this, &TcpClient::getTerritories, this, [this] () {
        m_mainWindow->getUi()->teMessages->setEnabled(true);
        m_mainWindow->getUi()->teMessages->setText("");
        auto leMessContent = currentTerritories.split(' ');
        for (auto teMesTer : leMessContent) {
            m_mainWindow->getUi()->teMessages->append(teMesTer);
        }

        sendMessage("#INSTANCELOOP");
    });

    connect(this, &TcpClient::startTurn, this, [this] () {
        m_mainWindow->show();
        playerButton->setEnabled(true);
        if (numOfTanks == 0) {
            auto territories = currentTerritories.split(' ');
            if (firstRound) {
                firstRound = false;
                numOfTanks = 19;
            }
            else {
                playerMove = true;
                numOfTanks = territories.size() / 3;
            }
            for (auto territory : territories) {
                QPushButton* button = this->findChild<QPushButton*>(territory);

                if(button) {
                     button->setEnabled(true);
                }
            }
        }
    });

    connect(this, &TcpClient::waitingUpdate, this, [this] (QString updateContent) {
        auto trimData = updateContent.split(' ');
        for (int j = 1 ; j < trimData.size() - 1 ; j+=2) {
            QPushButton* button = this->findChild<QPushButton*>(trimData[j]);
            button->setText(trimData[j+1]);
        }
    });

    connect(m_mainWindow->getUi()->pbNextPlayer, &QPushButton::clicked, this, [this] () {
       m_mainWindow->getUi()->pbNextPlayer->setDisabled(true);
       m_mainWindow->getUi()->pbPlayMove->setDisabled(true);
       m_mainWindow->getUi()->wAttack->hide();
       m_mainWindow->getUi()->wCubes->hide();
       playerButton->setDisabled(true);
       sendMessage("#NEXTPLAYER");
    });

    connect(m_mainWindow->getUi()->pbPlayMove, &QPushButton::clicked, this, [this] () {
        m_mainWindow->getUi()->wAttack->show();
//        m_mainWindow->getUi()->cbFrom->setEnabled(true);

        if (!cbToDone) {

            m_mainWindow->getUi()->cbFrom->clear();
//            cbToDone = true;
            auto forCBTerritoryContent = currentTerritories.split(' ');
            auto forCBTankNumber = currentTanksInOrder.split(' ');
            int terIndex = 0;
            for (terIndex = 0 ; terIndex < forCBTerritoryContent.size() ; terIndex++) {
                if (forCBTankNumber[terIndex].toInt() != 1) {
                    m_mainWindow->getUi()->cbFrom->addItem(forCBTerritoryContent[terIndex]);
                }
            }
        }
//        m_mainWindow->getUi()->cbFrom->setCurrentIndex(0);
//        sendMessage("#REQUESTNEIGHBOURS");
    });

    connect(m_mainWindow->getUi()->cbFrom, &QComboBox::currentTextChanged, this, [this] () {
        auto territory = m_mainWindow->getUi()->cbFrom->currentText();
        sendMessage("#REQUESTNEIGHBOURS " + territory);
    });

    connect(this, &TcpClient::updateCbTo, this, [this] (QString neighbours) {
        m_mainWindow->getUi()->cbTo->clear();
        m_mainWindow->getUi()->cbTo->setEnabled(true);
        auto neighbourContent = neighbours.split(' ');
        for (auto neighbour : neighbourContent) {
            m_mainWindow->getUi()->cbTo->addItem(neighbour);
        }
        m_mainWindow->getUi()->pbAttack->setEnabled(true);
    });

    connect(m_mainWindow->getUi()->pbAttack, &QPushButton::clicked, this, [this] () {
        auto attackTerritory = m_mainWindow->getUi()->cbFrom->currentText();
        auto defenceTerritory = m_mainWindow->getUi()->cbTo->currentText();

        m_mainWindow->getUi()->wCubes->setHidden(false);
        m_mainWindow->getUi()->wCards->setHidden(true);
        m_mainWindow->getUi()->wMoveTanks->setHidden(true);

        QPushButton* buttonAtkTerritory = this->findChild<QPushButton*>(attackTerritory);
        QString attackTerritoryTanks = buttonAtkTerritory->text();

        QPushButton* buttonDefTerritory = this->findChild<QPushButton*>(defenceTerritory);
        QString defenceTerritoryTanks = buttonDefTerritory->text();

        std::cout << "#THROUGHDICES " + attackTerritory.toStdString() + " " + attackTerritoryTanks.toStdString() +  " " + defenceTerritory.toStdString() + " " + defenceTerritoryTanks.toStdString() << std::endl;
        sendMessage("#THROUGHDICES " + attackTerritory + " " + attackTerritoryTanks +  " " + defenceTerritory + " " + defenceTerritoryTanks);
        m_mainWindow->getUi()->pbAttack->setDisabled(true);
        m_mainWindow->getUi()->wAttack->hide();
    });

    connect(this, &TcpClient::throwDiceUpdate, this, [this] (QString atkTerName, QString atkTerTanks, QString defTerName, QString defTerTanks, QString diceContent) {
        std::cout << atkTerName.toStdString() << " " << atkTerTanks.toStdString() << " " << defTerName.toStdString() << " " << defTerTanks.toStdString() << std::endl;

        QPushButton* atkTer = this->findChild<QPushButton*>(atkTerName);
        atkTer->setText(atkTerTanks);

        QPushButton* defTer = this->findChild<QPushButton*>(defTerName);
        defTer->setText(defTerTanks);

        auto updateNames = currentTerritories.split(' ');
        auto updateTanks = currentTanksInOrder.split(' ');
        QString curTerUpd = "";
        QString curTankUpd = "";
        for (int k = 0 ; k < updateNames.size() ; k++) {
            if (atkTerName == updateNames[k]) {
                updateTanks[k] = atkTerTanks;
            }
            if (defTerName == updateNames[k]) {
                updateTanks[k] = defTerTanks;
            }
            curTerUpd += updateNames[k] + " ";
            curTankUpd += updateTanks[k] + " ";
        }
        currentTerritories = curTerUpd.remove(curTerUpd.length() - 1, 1);
        currentTanksInOrder = curTankUpd.remove(curTankUpd.length() - 1, 1);


        auto dices = diceContent.split(' ');
        if(dices[0] != 0) {
            QPixmap red1("../images/dices/red_" + dices[0] + ".png");
            m_mainWindow->getUi()->lRedDice1->setPixmap(red1.scaled(61,61, Qt::KeepAspectRatio));
        }

        if (dices[1] != 0) {
            QPixmap red2("../images/dices/red_" + dices[1] + ".png");
            m_mainWindow->getUi()->lRedDice2->setPixmap(red2.scaled(61,61, Qt::KeepAspectRatio));
        }

        if (dices[2] != 0) {
            QPixmap red3("../images/dices/red_" + dices[2] + ".png");
            m_mainWindow->getUi()->lRedDice3->setPixmap(red3.scaled(61,61, Qt::KeepAspectRatio));
        }

        if (dices[3] != 0) {
            QPixmap blue1("../images/dices/blue_" + dices[3] + ".png");
            m_mainWindow->getUi()->lBlueDice1->setPixmap(blue1.scaled(61,61, Qt::KeepAspectRatio));
        }

        if(dices[4] != 0) {
            QPixmap blue2("../images/dices/blue_" + dices[4] + ".png");
            m_mainWindow->getUi()->lBlueDice2->setPixmap(blue2.scaled(61,61, Qt::KeepAspectRatio));
        }

        if(dices[5] != 0) {
            QPixmap blue3("../images/dices/blue_" + dices[5] + ".png");
            m_mainWindow->getUi()->lBlueDice3->setPixmap(blue3.scaled(61,61, Qt::KeepAspectRatio));
        }

//        cbToDone = false;
        sendMessage("#ATTACKUPDATE " + atkTerName + " " + atkTerTanks + " " + defTerName + " " + defTerTanks);
    });

    connect(this, &TcpClient::attackUpdate, this, [this] (QString atkTerName, QString atkTerTanks, QString defTerName, QString defTerTanks) {
        QPushButton* atkTer = this->findChild<QPushButton*>(atkTerName);
        atkTer->setText(atkTerTanks);

        QPushButton* defTer = this->findChild<QPushButton*>(defTerName);
        defTer->setText(defTerTanks);

        auto curTer = currentTerritories.split(' ');
        auto curTanks = currentTanksInOrder.split(' ');
        QString currentTerUpdate = "";
        QString currentTanksUpdate = "";
        for (int k = 0 ; k < curTer.size() ; k++) {
            if (defTerName == curTer[k]) {
                curTanks[k] = defTerTanks;
            }
            currentTerUpdate += curTer[k] + " ";
            currentTanksUpdate += curTanks[k] + " ";
        }
        currentTerritories = currentTerUpdate.remove(currentTerUpdate.length() - 1, 1);
        currentTanksInOrder = currentTanksUpdate.remove(currentTanksUpdate.length() - 1, 1);
    });

    connect(this, &TcpClient::territoryWon, this, [this] (QString atkTerName, QString atkTerTanks, QString newTerName, QString newTerTanks, QString diceContent) {
        QPushButton* atkTer = this->findChild<QPushButton*>(atkTerName);
        atkTer->setText(atkTerTanks);

        QPushButton* newTer = this->findChild<QPushButton*>(newTerName);
        newTer->setStyleSheet({"background-color: " + m_playerColor});
        newTer->setText(newTerTanks);

        currentTerritories += " " + newTerName;
        currentTanksInOrder += " " + newTerTanks;
        auto updateNames = currentTerritories.split(' ');
        auto updateTanks = currentTanksInOrder.split(' ');
        QString curTerUpd = "";
        QString curTankUpd = "";
        for (int k = 0 ; k < updateNames.size() ; k++) {
            if (atkTerName == updateNames[k]) {
                updateTanks[k] = atkTerTanks;
            }
            curTerUpd += updateNames[k] + " ";
            curTankUpd += updateTanks[k] + " ";
        }
        currentTerritories = curTerUpd.remove(curTerUpd.length() - 1, 1);
        currentTanksInOrder = curTankUpd.remove(curTankUpd.length() - 1, 1);


        auto dices = diceContent.split(' ');
        if(dices[0] != 0) {
            QPixmap red1("../images/dices/red_" + dices[0] + ".png");
            m_mainWindow->getUi()->lRedDice1->setPixmap(red1.scaled(61,61, Qt::KeepAspectRatio));
        }

        if (dices[1] != 0) {
            QPixmap red2("../images/dices/red_" + dices[1] + ".png");
            m_mainWindow->getUi()->lRedDice2->setPixmap(red2.scaled(61,61, Qt::KeepAspectRatio));
        }

        if (dices[2] != 0) {
            QPixmap red3("../images/dices/red_" + dices[2] + ".png");
            m_mainWindow->getUi()->lRedDice3->setPixmap(red3.scaled(61,61, Qt::KeepAspectRatio));
        }

        if (dices[3] != 0) {
            QPixmap blue1("../images/dices/blue_" + dices[3] + ".png");
            m_mainWindow->getUi()->lBlueDice1->setPixmap(blue1.scaled(61,61, Qt::KeepAspectRatio));
        }

        if(dices[4] != 0) {
            QPixmap blue2("../images/dices/blue_" + dices[4] + ".png");
            m_mainWindow->getUi()->lBlueDice2->setPixmap(blue2.scaled(61,61, Qt::KeepAspectRatio));
        }

        if(dices[5] != 0) {
            QPixmap blue3("../images/dices/blue_" + dices[5] + ".png");
            m_mainWindow->getUi()->lBlueDice3->setPixmap(blue3.scaled(61,61, Qt::KeepAspectRatio));
        }


        sendMessage("#ATTACKWON " + atkTerName + " " + atkTerTanks + " " + newTerName + " " + newTerTanks + " " + m_playerColor);
    });

    connect(this, &TcpClient::attackWon, this, [this] (QString atkTerName, QString atkTerTanks, QString newTerName, QString newTerTanks, QString newColor) {
        QPushButton* atkTer = this->findChild<QPushButton*>(atkTerName);
        atkTer->setText(atkTerTanks);

        QPushButton* newTer = this->findChild<QPushButton*>(newTerName);
        newTer->setStyleSheet({"background-color: " + newColor});
        newTer->setText(newTerTanks);

        auto curTer = currentTerritories.split(' ');
        auto curTanks = currentTanksInOrder.split(' ');
        QString currentTerUpdate = "";
        QString currentTanksUpdate = "";
        for (int k = 0 ; k < curTer.size() ; k++) {
            if (newTerName == curTer[k]) {
                continue;
            }
            currentTerUpdate += curTer[k] + " ";
            currentTanksUpdate += curTanks[k] + " ";
        }
        currentTerritories = currentTerUpdate.remove(currentTerUpdate.length() - 1, 1);
        currentTanksInOrder = currentTanksUpdate.remove(currentTanksUpdate.length() - 1, 1);
    });
}

TcpClient::~TcpClient()
{
    delete ui;
}

void TcpClient::readMessage()
{
    if (m_socket->state() != QAbstractSocket::ConnectedState)
            return;

    QByteArray buffer;
    QDataStream socketStream(m_socket);
    quint32 messageSize = 0;
    socketStream >> messageSize;

    int length = qint64(messageSize - sizeof(quint32));
    while (m_socket->bytesAvailable() < length)
        if (!m_socket->waitForReadyRead())
            return;

    buffer.resize(length - sizeof(quint32));
    socketStream.skipRawData(sizeof(quint32));
    socketStream.readRawData(buffer.data(), length - sizeof(quint32));

    auto logMessage = QString::fromUtf8(buffer);
    qDebug() << logMessage;

    parseMessage(logMessage);
}

void TcpClient::sendMessage(QString message)
{
    if (message.isEmpty() || m_socket->state() != QAbstractSocket::ConnectedState)
        return;

    QByteArray byteArray = message.toUtf8();
    QDataStream socketStream(m_socket);
    socketStream.setVersion(QDataStream::Qt_5_6);
    socketStream << byteArray;
}

void TcpClient::on_connect_clicked()
{
    if (m_socket->state() != QAbstractSocket::ConnectedState) {
        std::cout << "Connecting..." << std::endl;
        m_socket->connectToHost(ui->ipaddress->text(), ui->port->value());
        guiChange(QAbstractSocket::ConnectingState);
    }
}

void TcpClient::connectedToServer()
{
    std::cout << "Connection established." << std::endl;
    emit connection("#CONNECT");
}


void TcpClient::on_disconnect_clicked()
{
    if (m_socket->state() != QAbstractSocket::ConnectingState) {
        std::cout << "Disconnecting..." << std::endl;
    }
    m_socket->abort();
    guiChange(QAbstractSocket::UnconnectedState);
}

void TcpClient::disconnectedFromServer()
{
    std::cout << "Disconnected from Server." << std::endl;
    this->close();
    guiChange(QAbstractSocket::UnconnectedState);
}

void TcpClient::closeEvent(QCloseEvent *event){
    QWidget::closeEvent(event);
    emit closeGame();
}


void TcpClient::guiChange(QAbstractSocket::SocketState socketState)
{
    const bool unconnected = socketState == QAbstractSocket::UnconnectedState;
    ui->connect->setEnabled(unconnected);
    ui->ipaddress->setEnabled(unconnected);
    ui->port->setEnabled(unconnected);

    ui->disconnect->setEnabled(!unconnected);
}

void TcpClient::parseMessage(const QString &logMessage)
{
    auto messageContent = logMessage.split(' ');
    auto serverMessage = messageContent[0];
    std::cout << messageContent[0].toStdString() << std::endl;
    if (serverMessage == "#NEWCONNECTION") {
        emit startMenu();
    }
    else if (serverMessage == "#LOCKLOBBY"){
        emit lockLobby();
    }
    else if (serverMessage == "#DISABLEJOINGAME") {
        emit disableJoin();
    }
    else if (serverMessage == "#REMOVECOLOR") {
        emit removeColor(messageContent[1]);
    }
    else if (serverMessage == "#NAMEUNAVAILABLE") {
        emit nameUnavailable();
    }
    else if (serverMessage == "#COLORUNAVAILABLE") {
        emit colorUnavailable();
    }
    else if (serverMessage == "#ENABLESTART") {
        emit enableStart();
    }
    else if (serverMessage == "#DISABLENAMECOLORBACK") {
        emit disableNameAndColor();
    }
    else if (serverMessage == "#ENABLEBACK") {
        emit enableBack();
    }
    else if (serverMessage == "#CLOSECLIENT") {
        emit closeClient();
    }
    else if (serverMessage == "#ALLPLAYERSREADY") {
        bool gameInitiated = true;
        if (messageContent[1] == "false")
            gameInitiated = false;
        emit allPlayersReady(gameInitiated);
    }
    else if (serverMessage == "#UPDATEGAMEUI") {
        emit updateGameUi();
    }
    else if (serverMessage == "#INITUPDATE") {
        QString info = "";
        for (i = 0 ; i < 5 ; i++) {
            info += messageContent[1 + i * 9] + " ";
            info += messageContent[2 + i * 9] + " ";
            info += messageContent[3 + i * 9] + " ";
            info += messageContent[4 + i * 9] + " ";
            info += messageContent[5 + i * 9] + " ";
            info += messageContent[6 + i * 9] + " ";
            info += messageContent[7 + i * 9] + " ";
            info += messageContent[8 + i * 9] + " ";
            info += messageContent[9 + i * 9] + " ";
        }
//        info.remove(info.length() - 1, 1);
        info += messageContent[messageContent.size()-1];
        emit initUpdate(info);
    }
    else if (serverMessage == "#TASKCOLORINFO") {
        m_playerColor = messageContent[2].toLower();
        Color infoColor;
        if (messageContent[2] == "RED")
            infoColor = Color::RED;
        if (messageContent[2] == "GREEN")
            infoColor = Color::GREEN;
        if (messageContent[2] == "BLUE")
            infoColor = Color::BLUE;
        if (messageContent[2] == "YELLOW")
            infoColor = Color::YELLOW;
        if (messageContent[2] == "PURPLE")
            infoColor = Color::PURPLE;
        m_player = new Player(messageContent[1], infoColor, true);
        emit taskCardColor(messageContent[2]);
    }
    else if (serverMessage == "#REQUESTAPPROVED") {
        if (!reqapproved) {
            reqapproved = true;
            QString ter = "";
            currentTerritories += messageContent[1];
            for (int j = 2 ; j < messageContent.size() - 1 ; j++) {
    //            Territory *t = new Territory(messageContent[j]);
    //            t->setNumOfTanks(1);
    //            t->setOwner(m_player);
    //            m_player->addTerritory(t);
    //            ter += messageContent[j] + " ";
                currentTerritories += " " + messageContent[j];
            }
        }
        emit getTerritories();
    }
    else if (serverMessage == "#STARTLOOP") {
        std::cout << "Starting turn" << std::endl;
        emit startTurn();
    }
    else if (serverMessage == "#WAITINGUPDATE") {
        QString updateContent = "";
        for (int j = 0 ; j < messageContent.size() ; j++) {
            updateContent += messageContent[j] + " ";
        }
        updateContent.remove(updateContent.length() - 1, 1);
        emit waitingUpdate(updateContent);
    }
    else if (serverMessage == "#WAITING") {
        std::cout << "WAITING FOR CUR PLAYER" << std::endl;
        emit returnBreak();
    }
    else if (serverMessage == "#NEIGHBOURUPDATE") {
        QString msg = "";
        for (int k = 1 ; k < messageContent.size() ; k++) {
            msg += messageContent[k] + " ";
        }
        msg.remove(msg.length() - 1, 1);

        emit updateCbTo(msg);
    }
    else if (serverMessage == "#DRAWTHROWUPDATE") {
        QString diceContent = "";
        for (int k = 5 ; k < messageContent.size() ; k++) {
            diceContent += messageContent[k] + " ";
        }
        diceContent.remove(diceContent.length() - 1, 1);
        emit throwDiceUpdate(messageContent[1], messageContent[2], messageContent[3], messageContent[4], diceContent);
    }
    else if (serverMessage == "#ATTACKUPDATE") {
        emit attackUpdate(messageContent[1], messageContent[2], messageContent[3], messageContent[4]);
    }
    else if (serverMessage == "#TERRITORYWON") {
        QString diceContent = "";
        for (int k = 5 ; k < messageContent.size() ; k++) {
            diceContent += messageContent[k] + " ";
        }
        diceContent.remove(diceContent.length() - 1, 1);
        emit territoryWon(messageContent[1], messageContent[2], messageContent[3], messageContent[4], diceContent);
    }
    else if (serverMessage == "#ATTACKWON") {
        emit attackWon(messageContent[1], messageContent[2], messageContent[3], messageContent[4], messageContent[5]);
    }
}
