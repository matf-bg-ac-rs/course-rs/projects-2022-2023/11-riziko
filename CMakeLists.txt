cmake_minimum_required(VERSION 3.16)
project(risk LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC OFF)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt5Core REQUIRED)
find_package(Qt5Gui REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Multimedia REQUIRED)
find_package(Qt5MultimediaWidgets REQUIRED)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS}")
set(QT_USE_QTMULTIMEDIA TRUE)
set(QT_USE_QTMULTIMEDIAWIDGETS TRUE)

set(RESOURCES riziko/images.qrc)

qt5_add_big_resources(RES_SOURCES ${RESOURCES})

set(SOURCES
    riziko/card.cpp
    riziko/cardtask.cpp
    riziko/destroyplayercard.cpp
    riziko/game.cpp
    riziko/initializer.cpp
    riziko/main.cpp
    riziko/mainwindow.cpp
    riziko/player.cpp
    riziko/region.cpp
    riziko/regionoccupycard.cpp
    riziko/territory.cpp
    riziko/territoryoccupycard.cpp
)

set(HEADERS
    riziko/card.h
    riziko/cardtask.h
    riziko/destroyplayercard.h
    riziko/game.h
    riziko/initializer.h
    riziko/mainwindow.h
    riziko/player.h
    riziko/region.h
    riziko/regionoccupycard.h
    riziko/territory.h
    riziko/territoryoccupycard.h
)

set(UIS
     riziko/mainwindow.ui
)

qt5_wrap_ui(UI_HEADERS ${UIS})

add_executable(${PROJECT_NAME} ${HEADERS} ${SOURCES} ${UI_HEADERS} ${RES_SOURCES})
qt5_use_modules(${PROJECT_NAME} Core Gui Multimedia Widgets MultimediaWidgets)

