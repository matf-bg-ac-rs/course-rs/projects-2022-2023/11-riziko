#include "mainwindow.h"

#include <QRandomGenerator>
#include <QTimer>
#include <iostream>

#include "card.h"
#include "cardtask.h"
#include "game.h"
#include "initializer.h"
#include "player.h"
#include "territory.h"
#include "ui_mainwindow.h"

int numOfPlayers = 0;

class Card;
class Player;
class Region;
class Initializer;
class Game;

int indikator = -1;
QVector<Player*> players;

QVector<QString> names;

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
  this->setFixedSize(1119, 754);

  game = new Game();
  init = new Initializer();

  ui->setupUi(this);
  ui->stackedWidget->setCurrentIndex(0);

  ui->wCards->setHidden(true);
  ui->wCubes->setHidden(true);
  ui->wMoveTanks->setHidden(true);
  ui->pbDrawCard->setEnabled(false);
  ui->pbCards->setEnabled(false);
  ui->pbPlayMove->setEnabled(false);
  ui->pbExcangeCards->setEnabled(false);
  ui->lExcangeCard1->setHidden(true);
  ui->lExcangeCard2->setHidden(true);
  ui->lExcangeCard3->setHidden(true);
  ui->teMessages->setEnabled(false);

  ui->pbJoinGame->setFlat(true);
  ui->pbJoinGame->setStyleSheet("QPushButton { background-color: transparent }");

  ui->pbSettings->setFlat(true);
  ui->pbSettings->setStyleSheet("QPushButton { background-color: transparent }");

  ui->pbExit->setFlat(true);
  ui->pbExit->setStyleSheet("QPushButton { background-color: transparent }");

  ui->pbStart->setFlat(true);
  ui->pbStart->setStyleSheet("QPushButton { background-color: transparent }");

  ui->pbBack->setFlat(true);
  ui->pbBack->setStyleSheet("QPushButton { background-color: transparent }");

  connect(ui->cbFrom, SIGNAL(currentIndexChanged(QString)), this, SLOT(updateCbTo(QString)));
  connect(ui->cbFromMove, SIGNAL(currentIndexChanged(QString)), this, SLOT(updateCbNum(QString)));

  timer = new QTimer(this);

  connect(ui->pbStart, &QPushButton::clicked, this, &MainWindow::startTimer);
  connect(timer, &QTimer::timeout, this, &MainWindow::timerTimeout);
}

MainWindow::~MainWindow() {
  delete ui;
  delete game;
}

void MainWindow::on_pbJoinGame_clicked() { ui->stackedWidget->setCurrentIndex(1); }

void MainWindow::on_pbBack_clicked() { ui->stackedWidget->setCurrentIndex(0); }

void MainWindow::on_pbAddPlayer_clicked() {
  if (numOfPlayers == 5) {
    QMessageBox messageBox;
    messageBox.setWindowTitle("ERROR");
    messageBox.setText(
        "Već ste uneli potreban broj igrača. Možete pritisnuti START i započeti partiju!");
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.setStandardButtons(QMessageBox::Close);
    messageBox.setDefaultButton(QMessageBox::Ok);

    [[gnu::unused]] int number = messageBox.exec();
    ui->pbAddPlayer->setEnabled(false);
  } else {
    if (ui->leName->text() != "") {
      QString name = ui->leName->text();
      QString color = ui->cbColor->currentText();

      if (names.contains(name)) {
        QMessageBox messageBox;
        messageBox.setWindowTitle("ERROR");
        messageBox.setText("Ime je zauzeto! Molim Vas izaberite drugo!");
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.setStandardButtons(QMessageBox::Close);
        messageBox.setDefaultButton(QMessageBox::Ok);

        [[gnu::unused]] int number = messageBox.exec();
      }

      else {
        names.append(name);
        if (color == "RED") {
          Player* newPlayer = new Player();
          newPlayer->setName(name);
          newPlayer->setColor(Color::RED);
          ui->cbColor->removeItem(ui->cbColor->currentIndex());
          numOfPlayers++;
          players.append(newPlayer);
          ui->leName->setText("");
        }
        if (color == "BLUE") {
          Player* newPlayer = new Player();
          newPlayer->setName(name);
          newPlayer->setColor(Color::BLUE);
          ui->cbColor->removeItem(ui->cbColor->currentIndex());
          numOfPlayers++;
          players.append(newPlayer);
          ui->leName->setText("");
        }
        if (color == "YELLOW") {
          Player* newPlayer = new Player();
          newPlayer->setName(name);
          newPlayer->setColor(Color::YELLOW);
          ui->cbColor->removeItem(ui->cbColor->currentIndex());
          numOfPlayers++;
          players.append(newPlayer);
          ui->leName->setText("");
        }
        if (color == "GREEN") {
          Player* newPlayer = new Player();
          newPlayer->setName(name);
          newPlayer->setColor(Color::GREEN);
          ui->cbColor->removeItem(ui->cbColor->currentIndex());
          numOfPlayers++;
          players.append(newPlayer);
          ui->leName->setText("");
        }
        if (color == "PURPLE") {
          Player* newPlayer = new Player();
          newPlayer->setName(name);
          newPlayer->setColor(Color::PURPLE);
          ui->cbColor->removeItem(ui->cbColor->currentIndex());
          numOfPlayers++;
          players.append(newPlayer);
          ui->leName->setText("");
        }
      }
    } else {
      QMessageBox messageBox;
      messageBox.setWindowTitle("ERROR");
      messageBox.setText("Molimo Vas da unesete ime pre početka partije!");
      messageBox.setStandardButtons(QMessageBox::Ok);
      messageBox.setStandardButtons(QMessageBox::Close);
      messageBox.setDefaultButton(QMessageBox::Ok);

      [[gnu::unused]] int number = messageBox.exec();
    }
  }
}

void MainWindow::on_pbExit_clicked() { exit(0); }

void MainWindow::on_pbStart_clicked() {
  ui->wAttack->setHidden(true);

  if (numOfPlayers != 5) {
    QMessageBox messageBox;
    messageBox.setWindowTitle("ERROR");
    messageBox.setText("Morate uneti PET igrača pre početka partije!");
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.setStandardButtons(QMessageBox::Close);
    messageBox.setDefaultButton(QMessageBox::Ok);

    [[gnu::unused]] int number = messageBox.exec();
  } else {
    ui->pbPlayer1->setText(players[0]->name());
    QString color = players[0]->toString();
    ui->pbPlayer1->setStyleSheet({"background-color: " + color});

    ui->pbPlayer2->setText(players[1]->name());
    color = players[1]->toString();
    ui->pbPlayer2->setStyleSheet({"background-color: " + color});

    ui->pbPlayer3->setText(players[2]->name());
    color = players[2]->toString();
    ui->pbPlayer3->setStyleSheet({"background-color: " + color});

    ui->pbPlayer4->setText(players[3]->name());
    color = players[3]->toString();
    ui->pbPlayer4->setStyleSheet({"background-color: " + color});

    ui->pbPlayer5->setText(players[4]->name());
    color = players[4]->toString();
    ui->pbPlayer5->setStyleSheet({"background-color: " + color});

    game->setPlayers(players);

    game->setTerritories(init->initializerTerritory(players));

    game->setRegions(init->initializeRegions());

    game->setCards(init->initializeCards());

    init->initializeCardTasks(players);

    for (auto t : game->players()[0]->territories()) {
      QString color = game->players()[0]->toString();
      QPushButton* button = this->findChild<QPushButton*>(t->name());

      if (button) {
        button->setStyleSheet({"background-color: " + color});
        button->setText(QString::number(1));
      }
    }

    for (auto t : game->players()[1]->territories()) {
      QString color = game->players()[1]->toString();
      QPushButton* button = this->findChild<QPushButton*>(t->name());

      if (button) {
        button->setStyleSheet({"background-color: " + color});
        button->setText(QString::number(1));
      }
    }

    for (auto t : game->players()[2]->territories()) {
      QString color = game->players()[2]->toString();
      QPushButton* button = this->findChild<QPushButton*>(t->name());

      if (button) {
        button->setStyleSheet({"background-color: " + color});
        button->setText(QString::number(1));
      }
    }

    for (auto t : game->players()[3]->territories()) {
      QString color = game->players()[3]->toString();
      QPushButton* button = this->findChild<QPushButton*>(t->name());

      if (button) {
        button->setStyleSheet({"background-color: " + color});
        button->setText(QString::number(1));
      }
    }

    for (auto t : game->players()[4]->territories()) {
      QString color = game->players()[4]->toString();
      QPushButton* button = this->findChild<QPushButton*>(t->name());

      if (button) {
        button->setStyleSheet({"background-color: " + color});
        button->setText(QString::number(1));
      }
    }

    ui->stackedWidget->setCurrentIndex(2);
    indikator = 0;
    game->setCurrentPlayer(players[0]);

    for (auto t : game->players()[0]->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(true);
      }
    }

    QString path = game->currentPlayer()->cardTask()->path();
    QPixmap pix(path);
    ui->lTaskCard->setPixmap(pix.scaled(171, 171, Qt::KeepAspectRatio));
    QString boja = game->currentPlayer()->toString();
    ui->leTaskCardText->setStyleSheet({"background-color: " + boja});
    ui->teMessages->append(
        "Na redu je igrac " + game->currentPlayer()->name() + ". Njegove teritorije su: ");

    for (auto t : game->currentPlayer()->territories()) {
      ui->teMessages->append(t->name());
    }
  }
}

QSet<int> drawn_cards;
bool deck(QSet<int> drawn_cards) {
  for (int i = 1; i < 36; i++)
    if (!drawn_cards.contains(i)) return true;
  return false;
}

void MainWindow::on_pbDrawCard_clicked() {
  ui->wAttack->setHidden(true);

  ui->wCards->setHidden(false);

  ui->wCubes->setHidden(false);

  ui->wMoveTanks->setHidden(true);

  if (!deck(drawn_cards)) {
    QMessageBox messageBox;
    messageBox.setWindowTitle("ERROR");
    messageBox.setText("Nema dostupnih kartica za dodelu!");
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.setStandardButtons(QMessageBox::Close);
    messageBox.setDefaultButton(QMessageBox::Ok);

    [[gnu::unused]] int number = messageBox.exec();
  } else {
    int randomCard = QRandomGenerator::global()->bounded(1, 36);
    while (drawn_cards.contains(randomCard)) {
      randomCard = QRandomGenerator::global()->bounded(1, 36);
    }

    drawn_cards.insert(randomCard);

    QString path = "../images/territory_cards/" + QString::number(randomCard) + ".png";
    QPixmap territoryCard(path);
    ui->lCards->setPixmap(territoryCard.scaled(171, 171, Qt::KeepAspectRatio));

    game->currentPlayer()->addCard(Initializer::allCards[randomCard]);

    ui->pbPlayMove->setEnabled(false);

    ui->pbDrawCard->setEnabled(false);
  }
}

void MainWindow::on_pbAttack_clicked() {
  Territory* attTerritory;
  Territory* defTerritory;

  for (auto t : game->territories()) {
    if (t->name() == ui->cbFrom->currentText()) {
      attTerritory = t;
    } else if (t->name() == ui->cbTo->currentText()) {
      defTerritory = t;
    }
  }

  if (attTerritory->numOfTanks() == 1) {
    QMessageBox messageBox;
    messageBox.setWindowTitle("ERROR");
    messageBox.setText(
        "Ne možete napasti ukoliko imate samo jedan tenkić na teritoriji. Molimo Vas izaberite "
        "neku drugu teritoriju!");
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.setStandardButtons(QMessageBox::Close);
    messageBox.setDefaultButton(QMessageBox::Ok);

    [[gnu::unused]] int number = messageBox.exec();
  }

  else {
    ui->wCubes->setHidden(false);
    ui->wCards->setHidden(true);
    ui->wMoveTanks->setHidden(true);

    QVector<int> dices = game->attack(attTerritory, defTerritory);

    if (dices[0] != 0) {
      QPixmap red1("../images/dices/red_" + QString::number(dices[0]) + ".png");
      ui->lRedDice1->setPixmap(red1.scaled(61, 61, Qt::KeepAspectRatio));
    } else {
      ui->lRedDice1->clear();
    }

    if (dices[1] != 0) {
      QPixmap red2("../images/dices/red_" + QString::number(dices[1]) + ".png");
      ui->lRedDice2->setPixmap(red2.scaled(61, 61, Qt::KeepAspectRatio));
    } else {
      ui->lRedDice2->clear();
    }

    if (dices[2] != 0) {
      QPixmap red3("../images/dices/red_" + QString::number(dices[2]) + ".png");
      ui->lRedDice3->setPixmap(red3.scaled(61, 61, Qt::KeepAspectRatio));
    } else {
      ui->lRedDice3->clear();
    }

    if (dices[3] != 0) {
      QPixmap blue1("../images/dices/blue_" + QString::number(dices[3]) + ".png");
      ui->lBlueDice1->setPixmap(blue1.scaled(61, 61, Qt::KeepAspectRatio));
    } else {
      ui->lBlueDice1->clear();
    }

    if (dices[4] != 0) {
      QPixmap blue2("../images/dices/blue_" + QString::number(dices[4]) + ".png");
      ui->lBlueDice2->setPixmap(blue2.scaled(61, 61, Qt::KeepAspectRatio));
    } else {
      ui->lBlueDice2->clear();
    }

    if (dices[5] != 0) {
      QPixmap blue3("../images/dices/blue_" + QString::number(dices[5]) + ".png");
      ui->lBlueDice3->setPixmap(blue3.scaled(61, 61, Qt::KeepAspectRatio));
    } else {
      ui->lBlueDice3->clear();
    }

    dices.clear();

    QPushButton* button1 = this->findChild<QPushButton*>(attTerritory->name());
    if (button1) {
      button1->setText(QString::number(attTerritory->numOfTanks()));
    }

    QPushButton* button2 = this->findChild<QPushButton*>(defTerritory->name());
    if (button2) {
      button2->setText(QString::number(defTerritory->numOfTanks()));
    }

    if (defTerritory->numOfTanks() == 0) {
      QMessageBox messageBox;
      messageBox.setWindowTitle("BRAVO");
      messageBox.setText("Osvojili ste teritoriju: " + defTerritory->name());
      messageBox.setStandardButtons(QMessageBox::Ok);
      messageBox.setDefaultButton(QMessageBox::Ok);

      [[gnu::unused]] int number = messageBox.exec();

      defTerritory->owner()->removeTerritory(defTerritory);

      if (defTerritory->owner()->numOfTerritories() == 0) {  // nije usao
        game->removePlayer(defTerritory->owner());
        attTerritory->owner()->addTerritory(defTerritory);
        defTerritory->setOwner(attTerritory->owner());
      }

      attTerritory->owner()->addTerritory(defTerritory);
      defTerritory->setOwner(attTerritory->owner());

      ui->pbDrawCard->setEnabled(true);

      QString color = attTerritory->owner()->toString();
      button2->setStyleSheet({"background-color: " + color});

      button1->setText(QString::number((attTerritory->numOfTanks()) - 1));
      button2->setText(QString::number((defTerritory->numOfTanks()) + 1));

      for (auto t : game->currentPlayer()->territories()) {
        ui->cbFromMove->addItem(t->name());
      }

      ui->cbFrom->addItem(defTerritory->name());

      game->currentPlayer()->moveTanks(attTerritory, defTerritory, 1);

      ui->wAttack->setHidden(true);
      ui->wMoveTanks->setHidden(false);

      if (game->checkGoal(attTerritory->owner())) {
        QMessageBox messageBox;
        messageBox.setWindowTitle("WIN");
        messageBox.setText("Pobedio je igrac: " + attTerritory->owner()->name() + ".\nČESTITAMO!");
        messageBox.setIcon(QMessageBox::Information);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.setDefaultButton(QMessageBox::Ok);

        [[gnu::unused]] int number = messageBox.exec();

        exit(0);
      }
    }
  }
}

int last;
void MainWindow::on_pbCards_clicked() {
  ui->wAttack->setHidden(true);
  auto cards = game->currentPlayer()->cards();
  if (!cards.isEmpty()) {
    ui->pbNextCard->setEnabled(true);

    ui->pbNextCard->setHidden(false);
    ui->pbExitCards->setHidden(false);

    ui->wCubes->setHidden(false);
    ui->wCards->setHidden(false);
    auto it = cards.begin();
    int num = (*it)->id();

    QString path = "../images/territory_cards/" + QString::number(num) + ".png";
    QPixmap territoryCard(path);

    ui->wCards->setHidden(false);
    ui->lCards->setPixmap(territoryCard.scaled(171, 171, Qt::KeepAspectRatio));
    last = 0;

    if (cards.size() >= 3) {
      ui->pbExcangeCards->setEnabled(true);
    }
  } else {
    QMessageBox messageBox;
    messageBox.setWindowTitle("ERROR");
    messageBox.setText("Nemate kartica!");
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.setDefaultButton(QMessageBox::Ok);

    [[gnu::unused]] int number = messageBox.exec();

    ui->pbNextCard->setEnabled(false);
  }
}

void MainWindow::on_pbExitCards_clicked() { ui->wCubes->setHidden(true); }

void MainWindow::on_pbNextCard_clicked() {
  int length = game->currentPlayer()->cards().length();
  if (length == 1) {
    ui->pbNextCard->setEnabled(false);
  } else {
    int index = (last + 1) % length;

    int num = game->currentPlayer()->cards()[index]->id();
    QString path = "../images/territory_cards/" + QString::number(num) + ".png";
    QPixmap territoryCard(path);

    ui->wCards->setHidden(false);
    ui->lCards->setPixmap(territoryCard.scaled(171, 171, Qt::KeepAspectRatio));
    last = index;
    ui->pbNextCard->setEnabled(true);
  }
}

void MainWindow::on_pbPlayMove_clicked() {
  if (game->currentPlayer()->getNumOfTanks() != 0) {
    QMessageBox messageBox;
    messageBox.setWindowTitle("ERROR");
    messageBox.setText(
        "Niste rasporedili sve tenkiće. Molimo Vas da to uradite pre potencijalnog! Imate još " +
        QString::number(game->currentPlayer()->getNumOfTanks()) + " tenkića");
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.setDefaultButton(QMessageBox::Ok);

    [[gnu::unused]] int number = messageBox.exec();
  }

  else {
    ui->wAttack->setHidden(false);
    ui->cbFrom->clear();
    ui->cbTo->clear();

    for (auto t : game->currentPlayer()->territories()) {
      ui->cbFrom->addItem(t->name());
    }
  }

  ui->wMoveTanks->setHidden(true);
}

QVector<Card*> chosenCards;
int counter = 0;
void MainWindow::on_pbExcangeCards_clicked() {
  ui->wCubes->setHidden(false);
  ui->wCards->setHidden(false);
  auto card = game->currentPlayer()->cards()[last];
  if (counter == 3) {
    if (chosenCards[0]->id() == chosenCards[1]->id() ||
        chosenCards[1]->id() == chosenCards[2]->id() ||
        chosenCards[0]->id() == chosenCards[2]->id()) {
      QMessageBox messageBox;
      messageBox.setWindowTitle("ERROR");
      messageBox.setText("Kartice za razmenu moraju da budu različite!");
      messageBox.setStandardButtons(QMessageBox::Ok);
      messageBox.setDefaultButton(QMessageBox::Ok);

      [[gnu::unused]] int number = messageBox.exec();
      counter = 0;
    }

    int tanks = players[0]->getTanks(chosenCards[0], chosenCards[1], chosenCards[2]);

    if (tanks != -1) {
      for (auto c : players[0]->cards()) {
        if (c->id() == chosenCards[0]->id() || c->id() == chosenCards[1]->id() ||
            c->id() == chosenCards[2]->id()) {
          players[0]->removeCard(c);
        }
      }

      drawn_cards.remove(chosenCards[0]->id());
      drawn_cards.remove(chosenCards[1]->id());
      drawn_cards.remove(chosenCards[2]->id());

      counter = 0;
      chosenCards.clear();
      QMessageBox messageBox;
      messageBox.setWindowTitle("BRAVO");
      messageBox.setText(
          "Uspešno ste razmenili kartice i dobili:  " + QString::number(tanks) + " tenkića!");
      messageBox.setStandardButtons(QMessageBox::Ok);
      messageBox.setDefaultButton(QMessageBox::Ok);

      [[gnu::unused]] int number = messageBox.exec();

      game->currentPlayer()->setNumOfTanks(game->currentPlayer()->getNumOfTanks() + tanks);

      for (auto t : game->currentPlayer()->territories()) {
        QPushButton* button = this->findChild<QPushButton*>(t->name());
        if (button) {
          button->setEnabled(true);
        }
      }

    } else {
      chosenCards.clear();
      QMessageBox messageBox;
      messageBox.setWindowTitle("ERROR");
      messageBox.setText("Niste razmenili kartice!");
      messageBox.setStandardButtons(QMessageBox::Ok);
      messageBox.setDefaultButton(QMessageBox::Ok);

      [[gnu::unused]] int number = messageBox.exec();
      counter = 0;
    }

    ui->wCubes->setHidden(true);
    ui->lCards->setHidden(false);
    ui->lExcangeCard1->setHidden(true);
    ui->lExcangeCard2->setHidden(true);
    ui->lExcangeCard3->setHidden(true);

    counter = 0;

    ui->pbExcangeCards->setEnabled(false);

  } else {
    chosenCards.push_back(card);
    counter++;
    QString path = "../images/territory_cards/" + QString::number(card->id()) + ".png";
    QPixmap territoryCard(path);
    if (counter == 1) {
      ui->lExcangeCard1->setPixmap(territoryCard.scaled(131, 171, Qt::KeepAspectRatio));
    } else if (counter == 2) {
      ui->lExcangeCard2->setPixmap(territoryCard.scaled(131, 171, Qt::KeepAspectRatio));
    } else if (counter == 3) {
      ui->lExcangeCard3->setPixmap(territoryCard.scaled(131, 171, Qt::KeepAspectRatio));
      ui->lExcangeCard1->setHidden(false);
      ui->lExcangeCard2->setHidden(false);
      ui->lExcangeCard3->setHidden(false);
      ui->lCards->setHidden(true);
      ui->pbNextCard->setHidden(true);
      ui->pbExitCards->setHidden(true);
    }
  }
}

void MainWindow::updateCbTo(QString n) {
  ui->cbTo->clear();

  for (auto t : game->territories()) {
    if (t->name() == n) {
      for (auto neighbour : t->neighborTerritories()) {
        if (neighbour->owner()->name() != game->currentPlayer()->name()) {
          ui->cbTo->addItem(neighbour->name());
        }
      }
    }
  }
}

void MainWindow::updateCbNum(QString n) {
  ui->cbNum->clear();

  for (auto t : game->territories()) {
    if (t->name() == n) {
      for (int i = 0; i < t->numOfTanks(); i++) {
        ui->cbNum->addItem(QString::number(i));
      }
    }
  }
}

void MainWindow::startTimer() { timer->start(300000); }

void MainWindow::timerTimeout() {
  QMessageBox messageBox;
  messageBox.setWindowTitle("TIME");
  messageBox.setText(
      "Igrate potez već pet minuta. Molimo Vas završite potez klikom na NEXT PLAYER.");
  messageBox.setStandardButtons(QMessageBox::Ok);
  messageBox.setDefaultButton(QMessageBox::Ok);

  [[gnu::unused]] int number = messageBox.exec();
}

bool firstRoundDone = false;
bool nextRound = false;
int i = 0;
void MainWindow::on_pbNextPlayer_clicked() {
  if (game->currentPlayer()->getNumOfTanks() != 0) {
    QMessageBox messageBox;
    messageBox.setWindowTitle("ERROR");
    messageBox.setText(
        "Niste rasporedili sve tenkiće. Molimo Vas da to uradite pre prelaska na sledećeg igrača! "
        "Imate još " +
        QString::number(game->currentPlayer()->getNumOfTanks()) + " tenkića");
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.setDefaultButton(QMessageBox::Ok);

    [[gnu::unused]] int number = messageBox.exec();

  }

  else {
    if (game->currentPlayer()->cards().size() != 0) {
      ui->pbCards->setEnabled(true);
    } else {
      ui->pbExcangeCards->setEnabled(false);
    }

    timer->stop();

    ui->pbDrawCard->setEnabled(false);
    ui->wMoveTanks->setHidden(true);
    ui->wCards->setHidden(true);
    ui->wAttack->setHidden(true);

    bool endGame = false;
    ;

    for (auto p : game->players()) {
      if (game->checkGoal(p)) {
        QMessageBox messageBox;
        messageBox.setWindowTitle("WIN");
        messageBox.setText("Pobedio je igrac: " + p->name() + ".\nČESTITAMO!");
        messageBox.setIcon(QMessageBox::Information);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.setDefaultButton(QMessageBox::Ok);

        [[gnu::unused]] int number = messageBox.exec();

        exit(0);

        endGame = true;
      }
    }

    if (!endGame) {
      if (indikator == game->players().size() - 1) {
        firstRoundDone = true;
        nextRound = true;
        i++;
        indikator = 0;
        ui->pbCards->setEnabled(true);
      } else {
        indikator += 1;
      }

      if (firstRoundDone) {
        ui->pbPlayMove->setEnabled(true);
      }

      if (nextRound && i > 1) {
        for (auto player : game->players()) {
          player->setNumOfTanks(player->territories().size() / 3);
        }
        nextRound = false;
      }

      game->setCurrentPlayer(game->players()[indikator]);

      if (game->currentPlayer()->getNumOfTanks() != 0) {
        for (auto t : game->currentPlayer()->territories()) {
          QPushButton* button = this->findChild<QPushButton*>(t->name());
          if (button) {
            button->setEnabled(true);
          }
        }
      }

      QString path = game->currentPlayer()->cardTask()->path();
      QPixmap pix(path);
      ui->lTaskCard->setPixmap(pix.scaled(171, 171, Qt::KeepAspectRatio));
      QString boja = game->currentPlayer()->toString();
      ui->leTaskCardText->setStyleSheet({"background-color: " + boja});
      ui->teMessages->setText("");
      ui->teMessages->append(
          "Na redu je igrac " + game->currentPlayer()->name() + ". Njegove teritorije su: ");

      ui->cbFrom->clear();

      for (auto t : game->currentPlayer()->territories()) {
        ui->teMessages->append(t->name());
        ui->cbFrom->addItem(t->name());
      }

      ui->cbTo->clear();
      ui->cbFromMove->clear();
      connect(ui->cbFrom, SIGNAL(currentIndexChanged(QString)), this, SLOT(updateCbTo(QString)));
      timer->start();
      ui->wCubes->setHidden(true);
    }
  }
}

void MainWindow::on_Albania_clicked() {
  QString buttonText = ui->Albania->text();
  int buttonNumber = buttonText.toInt();
  ui->Albania->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Albania") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Austria_clicked() {
  QString buttonText = ui->Austria->text();
  int buttonNumber = buttonText.toInt();
  ui->Austria->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->currentPlayer()->territories()) {
    if (t->name() == "Austria") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Estonia_clicked() {
  QString buttonText = ui->Estonia->text();
  int buttonNumber = buttonText.toInt();
  ui->Estonia->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Estonia") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Belarus_clicked() {
  QString buttonText = ui->Belarus->text();
  int buttonNumber = buttonText.toInt();
  ui->Belarus->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Belarus") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Belgium_clicked() {
  QString buttonText = ui->Belgium->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Belgium->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Belgium") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Bosnia_clicked() {
  QString buttonText = ui->Bosnia->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Bosnia->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Bosnia") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Bulgaria_clicked() {
  QString buttonText = ui->Bulgaria->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Bulgaria->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Bulgaria") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Croatia_clicked() {
  QString buttonText = ui->Croatia->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Croatia->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Croatia") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Czech_clicked() {
  QString buttonText = ui->Czech->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Czech->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Czech") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Denmark_clicked() {
  QString buttonText = ui->Denmark->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Denmark->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Denmark") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Finland_clicked() {
  QString buttonText = ui->Finland->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Finland->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Finland") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_France_clicked() {
  QString buttonText = ui->France->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->France->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "France") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Germany_clicked() {
  QString buttonText = ui->Germany->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Germany->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Germany") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Greece_clicked() {
  QString buttonText = ui->Greece->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Greece->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Greece") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Hungary_clicked() {
  QString buttonText = ui->Hungary->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Hungary->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Hungary") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Iceland_clicked() {
  QString buttonText = ui->Iceland->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Iceland->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Iceland") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Ireland_clicked() {
  QString buttonText = ui->Ireland->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Ireland->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Ireland") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Italy_clicked() {
  QString buttonText = ui->Italy->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Italy->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Italy") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Latvia_clicked() {
  QString buttonText = ui->Latvia->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Latvia->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Latvia") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Lithuania_clicked() {
  QString buttonText = ui->Lithuania->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Lithuania->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Lithuania") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Macedonia_clicked() {
  QString buttonText = ui->Macedonia->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Macedonia->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Macedonia") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Moldova_clicked() {
  QString buttonText = ui->Moldova->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Moldova->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Moldova") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Netherlands_clicked() {
  QString buttonText = ui->Netherlands->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Netherlands->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Netherlands") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Norway_clicked() {
  QString buttonText = ui->Norway->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Norway->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Norway") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Poland_clicked() {
  QString buttonText = ui->Poland->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Poland->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Poland") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Portugal_clicked() {
  QString buttonText = ui->Portugal->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Portugal->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Portugal") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Romania_clicked() {
  QString buttonText = ui->Romania->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Romania->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Romania") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Serbia_clicked() {
  QString buttonText = ui->Serbia->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Serbia->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Serbia") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Slovakia_clicked() {
  QString buttonText = ui->Slovakia->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Slovakia->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Slovakia") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Slovenia_clicked() {
  QString buttonText = ui->Slovenia->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Slovenia->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Slovenia") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Spain_clicked() {
  QString buttonText = ui->Spain->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Spain->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Spain") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Sweden_clicked() {
  QString buttonText = ui->Sweden->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Sweden->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Sweden") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Switzerland_clicked() {
  QString buttonText = ui->Switzerland->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Switzerland->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Switzerland") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_Ukraine_clicked() {
  QString buttonText = ui->Ukraine->text();
  int buttonNumber = buttonText.toInt();
  //buttonNumber++;
  ui->Ukraine->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "Ukraine") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_UnitedKingdom_clicked() {
  QString buttonText = ui->UnitedKingdom->text();
  int buttonNumber = buttonText.toInt();
  ui->UnitedKingdom->setText(QString::number(++buttonNumber));
  int numOfTanks = game->currentPlayer()->getNumOfTanks();
  game->currentPlayer()->setNumOfTanks(--numOfTanks);

  for (auto t : game->territories()) {
    if (t->name() == "UnitedKingdom") {
      t->increaseNumOfTanks(1);
    }
  }

  if (game->currentPlayer()->getNumOfTanks() == 0) {
    for (auto t : game->currentPlayer()->territories()) {
      QPushButton* button = this->findChild<QPushButton*>(t->name());
      if (button) {
        button->setEnabled(false);
      }
    }
  }
}

void MainWindow::on_pbMove_clicked() {
  Territory* terr1;
  for (auto t : game->territories()) {
    if (t->name() == ui->cbFromMove->currentText()) {
      terr1 = t;
    }
  }

  Territory* terr2;
  for (auto t : game->territories()) {
    if (t->name() == ui->cbTo->currentText()) {
      terr2 = t;
    }
  }

  int number = ui->cbNum->currentText().toInt();

  if (terr1->name() == terr2->name()) {
    QMessageBox messageBox;
    messageBox.setWindowTitle("ERROR");
    messageBox.setText("Ne možete da prebacuje sa teritorije koju ste upravo osvojili!");
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.setStandardButtons(QMessageBox::Close);
    messageBox.setDefaultButton(QMessageBox::Ok);

    [[gnu::unused]] int number = messageBox.exec();
  }

  else {
    game->currentPlayer()->moveTanks(terr1, terr2, number);

    QPushButton* button1 = this->findChild<QPushButton*>(terr1->name());
    if (button1) {
      button1->setText(QString::number(terr1->numOfTanks()));
    }

    QPushButton* button2 = this->findChild<QPushButton*>(terr2->name());
    if (button2) {
      button2->setText(QString::number(terr2->numOfTanks()));
    }

    ui->cbFromMove->clear();
    ui->cbNum->clear();
    ui->wMoveTanks->setHidden(true);
  }
}
