#include "territoryoccupycard.h"

#include "player.h"

TerritoryOccupyCard::TerritoryOccupyCard() {}

std::int32_t TerritoryOccupyCard::numOfTerritories() const { return m_numOfTerritories; }

bool TerritoryOccupyCard::checkTask(Player *playerToCheck) const {
  if (playerToCheck->territories().size() >= m_numOfTerritories) {
    return true;
  } else {
    return false;
  }
}

TerritoryOccupyCard::TerritoryOccupyCard(const QString &path, int32_t numOfTerritories)
    : CardTask(path), m_numOfTerritories(numOfTerritories) {}
