#include "destroyplayercard.h"

DestroyPlayerCard::DestroyPlayerCard() {}

Player *DestroyPlayerCard::playerToDestroy() const { return m_playerToDestroy; }

bool DestroyPlayerCard::checkTask(Player *playerToCheck) const {
  if (playerToCheck == playerToCheck->playerToDestroy()) {
    if (playerToCheck->territories().size() == 22) {
      return true;
    }
  } else {
    if (playerToCheck->playerToDestroy()->territories().size() == 0) {
      return true;
    }
  }

  return false;
}

DestroyPlayerCard::DestroyPlayerCard(const QString &path, Player *playerToDestroy)
    : CardTask(path), m_playerToDestroy(playerToDestroy) {}

DestroyPlayerCard::~DestroyPlayerCard() {}
