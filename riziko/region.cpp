#include "region.h"

Region::Region() {}

Region::Region(
    const QString &name, const QSet<Territory *> &territories, int32_t numOfTanksPerRound)
    : m_name(name), m_territories(territories), m_numOfTanksPerRound(numOfTanksPerRound) {}

Region::Region(const QString &name, const QSet<Territory *> &territories)
    : m_name(name), m_territories(territories) {}

const QString &Region::name() const { return m_name; }

const QSet<Territory *> &Region::territories() const { return m_territories; }

std::int32_t Region::numOfTanksPerRound() const { return m_numOfTanksPerRound; }

bool Region::regionOwner(Player *player) {
  for (auto &territory : m_territories)
    if (territory->owner() != player) return false;

  return true;
}
