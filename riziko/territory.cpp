#include "territory.h"

Territory::Territory() {}

const QString &Territory::name() const { return m_name; }

int32_t Territory::numOfTanks() const { return m_numOfTanks; }

const QSet<Territory *> &Territory::neighborTerritories() const { return m_neighborTerritories; }

void Territory::setNeighborTerritories(const QSet<Territory *> &territories) {
  for (auto t : territories) m_neighborTerritories.insert(t);
}

void Territory::setNumOfTanks(int numberOfTanks) { m_numOfTanks = numberOfTanks; }

void Territory::setTerritories(const QSet<Territory *> &territories) {
  m_neighborTerritories = territories;
}

void Territory::increaseNumOfTanks(std::int32_t num) { m_numOfTanks += num; }

Player *Territory::owner() const { return m_owner; }

void Territory::setOwner(Player *newOwner) { m_owner = newOwner; }

Territory::Territory(const QString &name) : m_name(name), m_numOfTanks(1) {}

Territory::~Territory() {}

bool Territory::decreaseNumOfTanks(int32_t num) {
  if (num > m_numOfTanks) return false;
  m_numOfTanks -= num;
  return true;
}
