#include "catch.hpp"

#include "../riziko/card.h"
#include "../riziko/cardtask.h"
#include "../riziko/destroyplayercard.h"
#include "../riziko/game.h"
#include "../riziko/initializer.h"
#include "../riziko/player.h"
#include "../riziko/region.h"
#include "../riziko/regionoccupycard.h"
#include "../riziko/territory.h"
#include "../riziko/territoryoccupycard.h"

TEST_CASE("Initialization of territories and tanks", "[player]") {

    Game *game = new Game();
    Initializer *init = new Initializer();
    Player *p1 = new Player("1", Color::RED);
    Player *p2 = new Player("2", Color::BLUE);
    Player *p3 = new Player("3", Color::PURPLE);
    Player *p4 = new Player("4", Color::YELLOW);
    Player *p5 = new Player("5", Color::GREEN);

    QVector<Player*> players{p1,p2,p3,p4,p5};

    int numOfTerritories = 7;
    int numOfTanks = 21;

    SECTION("Number of territories added to every player") {

        game->setTerritories(init->initializerTerritory(players));
        for(int i = 0; i < players.size(); i++) {
            REQUIRE(players[i]->territories().size() == numOfTerritories);
        }
    }

    SECTION("Number of tanks added to every player") {

        game->setTerritories(init->initializerTerritory(players));
        for(int i = 0; i < players.size(); i++) {
            REQUIRE(players[i]->getNumOfTanks() == numOfTanks);
        }
    }

    SECTION("First and second player doesn't have the same territory") {

        game->setTerritories(init->initializerTerritory(players));
        REQUIRE_FALSE(players[0]->territories().intersects(players[1]->territories()));
    }

    SECTION("Second and third player doesn't have the same territory") {

        game->setTerritories(init->initializerTerritory(players));
        REQUIRE_FALSE(players[1]->territories().intersects(players[2]->territories()));
    }

    SECTION("Third and fourth player doesn't have the same territory") {

        game->setTerritories(init->initializerTerritory(players));
        REQUIRE_FALSE(players[2]->territories().intersects(players[3]->territories()));
    }

    SECTION("Fourth and fifth player doesn't have the same territory") {

        game->setTerritories(init->initializerTerritory(players));
        REQUIRE_FALSE(players[3]->territories().intersects(players[4]->territories()));
    }

    SECTION("First and fifth player doesn't have the same territory") {

        game->setTerritories(init->initializerTerritory(players));
        REQUIRE_FALSE(players[0]->territories().intersects(players[4]->territories()));
    }

    SECTION("Neighbours of Bosnia"){
        QSet<QString> bosniaNeighbours = {"Serbia", "Croatia"};
        QSet<QString> neighborTerritories;

        game->setTerritories(init->initializerTerritory(players));

        for(int i = 0; i < players.size(); i++) {
            for(auto t : players[i]->territories()){
                if(t->name() == "Bosnia"){
                    for(auto n : t->neighborTerritories())
                        neighborTerritories.insert(n->name());
                break;
                }
            }
        }
        REQUIRE(neighborTerritories == bosniaNeighbours);
    }

    SECTION("Neighbours of Spain"){
        QSet<QString> spainNeighbours = {"Portugal", "Ireland", "France", "Italy"};
        QSet<QString> neighborTerritories;

        game->setTerritories(init->initializerTerritory(players));

        for(int i = 0; i < players.size(); i++) {
            for(auto t : players[i]->territories()){
                if(t->name() == "Spain"){
                    for(auto n : t->neighborTerritories())
                        neighborTerritories.insert(n->name());
                break;
                }
            }
        }
        REQUIRE(neighborTerritories == spainNeighbours);
    }

    SECTION("Neighbours of Netherlands"){
        QSet<QString> netherlandsNeighbours = {"Belgium", "UnitedKingdom", "Germany"};
        QSet<QString> neighborTerritories;

        game->setTerritories(init->initializerTerritory(players));

        for(int i = 0; i < players.size(); i++) {
            for(auto t : players[i]->territories()){
                if(t->name() == "Netherlands"){
                    for(auto n : t->neighborTerritories())
                        neighborTerritories.insert(n->name());
                break;
                }
            }
        }
        REQUIRE(neighborTerritories == netherlandsNeighbours);
    }

    SECTION("Neighbours of Finland"){
        QSet<QString> finlandNeighbours = {"Estonia", "Sweden", "Norway"};
        QSet<QString> neighborTerritories;

        game->setTerritories(init->initializerTerritory(players));

        for(int i = 0; i < players.size(); i++) {
            for(auto t : players[i]->territories()){
                if(t->name() == "Finland"){
                    for(auto n : t->neighborTerritories())
                        neighborTerritories.insert(n->name());
                break;
                }
            }
        }
        REQUIRE(neighborTerritories == finlandNeighbours);
    }

    SECTION("Neighbours of Belarus"){
        QSet<QString> belarusNeighbours = {"Ukraine", "Poland", "Lithuania", "Latvia"};
        QSet<QString> neighborTerritories;

        game->setTerritories(init->initializerTerritory(players));

        for(int i = 0; i < players.size(); i++) {
            for(auto t : players[i]->territories()){
                if(t->name() == "Belarus"){
                    for(auto n : t->neighborTerritories())
                        neighborTerritories.insert(n->name());
                break;
                }
            }
        }
        REQUIRE(neighborTerritories == belarusNeighbours);
    }
}

TEST_CASE("Initialize of Regions", "[Region]"){

    Initializer *init = new Initializer();
    QVector<Region*> regions = init->initializeRegions();
    QSet<QString> balctic = {"Finland", "Estonia", "Lithuania", "Latvia"};
    QSet<QString> centralEurope = {"Germany", "Poland", "Czech", "Austria", "Hungary", "Slovakia", "Switzerland"};
    QSet<QString> westernEurope = {"France", "Belgium", "Netherlands", "UnitedKingdom", "Ireland"};
    QSet<QString> scandinavia = {"Sweden", "Norway", "Denmark", "Iceland"};
    QSet<QString> easternEurope = {"Belarus", "Ukraine", "Moldova", "Romania", "Bulgaria"};

    SECTION("Balctic Region initialized correct"){
        QSet<QString> ter;
        for(auto t : regions[0]->territories())
            ter.insert(t->name());
        REQUIRE(ter == balctic);
    }

    SECTION("Central Europe  Region initialized correct"){
        QSet<QString> ter;
        for(auto t : regions[2]->territories())
            ter.insert(t->name());
        REQUIRE(ter == centralEurope);
    }

    SECTION("Western Europe Region initialized correct"){
        QSet<QString> ter;
        for(auto t : regions[3]->territories())
            ter.insert(t->name());
        REQUIRE(ter == westernEurope);
    }

    SECTION("Scandinavia Region initialized correct"){
        QSet<QString> ter;
        for(auto t : regions[4]->territories())
            ter.insert(t->name());
        REQUIRE(ter == scandinavia);
    }

    SECTION("Eastern Europe Region initialized correct"){
        QSet<QString> ter;
        for(auto t : regions[5]->territories())
            ter.insert(t->name());
        REQUIRE(ter == easternEurope);
    }
}
