QT = gui

TEMPLATE = app

CONFIG += c++17

#isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR=$$(CATCH_INCLUDE_DIR)
#!isEmpty(CATCH_INCLUDE_DIR): INCLUDEPATH *= $${CATCH_INCLUDE_DIR}

#isEmpty(CATCH_INCLUDE_DIR): {
#    message("CATCH_INCLUDE_DIR is not set, assuming Catch2 can be found automatically in your system")
#}

PROJECT_DIR = $$PWD/../riziko
INCLUDEPATH += $$PROJECT_DIR

FORMS += \
    $$PROJECT_DIR/mainwindow.ui


SOURCES +=     main.cpp     \
    testMaja.cpp \
    testNebojsa.cpp \
    testOgnjen.cpp \
    testTamara.cpp \
    ../riziko/card.cpp \
    ../riziko/cardtask.cpp \
    ../riziko/destroyplayercard.cpp \
    ../riziko/game.cpp \
    ../riziko/initializer.cpp \
    ../riziko/player.cpp \
    ../riziko/region.cpp \
    ../riziko/regionoccupycard.cpp \
    ../riziko/territory.cpp \
    ../riziko/territoryoccupycard.cpp \
    testUros.cpp

HEADERS += \
    catch.hpp \
    ../riziko/card.h \
    ../riziko/cardtask.h \
    ../riziko/destroyplayercard.h \
    ../riziko/game.h \
    ../riziko/initializer.h \
    ../riziko/player.h \
    ../riziko/region.h \
    ../riziko/regionoccupycard.h \
    ../riziko/territory.h \
    ../riziko/territoryoccupycard.h

TARGET = Test


