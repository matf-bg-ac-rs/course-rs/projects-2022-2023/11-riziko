#include "catch.hpp"

#include "../riziko/card.h"
#include "../riziko/destroyplayercard.h"
#include "../riziko/regionoccupycard.h"
#include "../riziko/territoryoccupycard.h"
#include "../riziko/player.h"
#include "../riziko/territory.h"

Player *p = new Player("Player", Color::RED);

Territory* t1 = new Territory("territory1");
Territory* t2 = new Territory("territory2");
Territory* t3 = new Territory("territory3");

Card* c1 = new Card(1, t1);
Card* c2 = new Card(2, t2);
Card* c3 = new Card(3, t3);

TEST_CASE("Player constructor check", "[player]") {
    REQUIRE(p->name()=="Player");
    REQUIRE(p->color()==Color::RED);
}

TEST_CASE("Set player name", "[player]") {
    //arrange
    QString name = "Nebojsa";
    //act
    p->setName(name);
    //assert
    CHECK(p->name()==name);
}

TEST_CASE("Set player color", "[player]") {

    SECTION("Blue color check") {
        p->setColor(Color::BLUE);
        CHECK(p->color()==Color::BLUE);
    }

    SECTION("Green color check") {
        p->setColor(Color::GREEN);
        CHECK(p->color()==Color::GREEN);
    }

    SECTION("Yellow color check") {
        p->setColor(Color::YELLOW);
        CHECK(p->color()==Color::YELLOW);
    }

    SECTION("Purple color check") {
        p->setColor(Color::PURPLE);
        CHECK(p->color()==Color::PURPLE);
    }

    SECTION("Red color check") {
        p->setColor(Color::RED);
        CHECK(p->color()==Color::RED);
    }
}

TEST_CASE("Set player to destroy", "[player]") {

    Player *player = new Player("player", Color::BLUE);
    p->setPlayerToDestroy(player);
    REQUIRE(p->playerToDestroy()==player);
}

TEST_CASE("Adding player's territories", "[player][territory]") {

    p->addTerritory(t1);
    p->addTerritory(t2);
    p->addTerritory(t3);
    auto territories = p->territories();

    REQUIRE_FALSE(territories.find(t1)==territories.end());
    REQUIRE_FALSE(territories.find(t2)==territories.end());
    REQUIRE_FALSE(territories.find(t3)==territories.end());
}

TEST_CASE("Removing player's territory", "[player][territory]") {

    p->addTerritory(t1);
    p->addTerritory(t2);
    p->addTerritory(t3);

    p->removeTerritory(t2);
    auto territories = p->territories();

    REQUIRE(territories.contains(t1));
    REQUIRE_FALSE(territories.contains(t2));
    REQUIRE(territories.contains(t3));
}

TEST_CASE("Removing all player's territories", "[player][territory]") {

    p->addTerritory(t1);
    p->addTerritory(t2);
    p->addTerritory(t3);

    p->removeAllTerritories();
    auto territories = p->territories();

    REQUIRE(territories.empty());
}

TEST_CASE("Set player's territories", "[player][territory]") {

    QSet<Territory *> terrs;
    terrs.insert(t1);
    terrs.insert(t2);
    terrs.insert(t3);

    p->setTerritories(terrs);
    auto territories = p->territories();

    REQUIRE_FALSE(territories.find(t1)==territories.end());
    REQUIRE_FALSE(territories.find(t2)==territories.end());
    REQUIRE_FALSE(territories.find(t3)==territories.end());
}

TEST_CASE("Number of territories", "[player][territory]") {
    //arrange
    p->addTerritory(t1);
    p->addTerritory(t2);
    p->addTerritory(t3);

    std::int32_t expectedValue = 3;
    //act
    std::int32_t returnValue = p->numOfTerritories();
    //assert
    REQUIRE(expectedValue==returnValue);
}

TEST_CASE("Adding player's card", "[player][card]") {

    p->addCard(c1);
    p->addCard(c2);
    p->addCard(c3);
    auto cards = p->cards();

    REQUIRE(cards[0]->id()==c1->id());
    REQUIRE(cards[1]->id()==c2->id());
    REQUIRE(cards[2]->id()==c3->id());
}

TEST_CASE("Removing player's card", "[player][card]") {

    p->addCard(c1);
    p->addCard(c2);
    p->addCard(c3);
    auto cards = p->cards();
    p->removeCard(c1);

    bool expectedValue = false;
    bool returnValue = false;
    for(auto c : cards)
        if(c->id()==c1->id())
            returnValue = true;

    REQUIRE_FALSE(expectedValue == returnValue);
    REQUIRE(cards[1]->id()==c2->id());
    REQUIRE(cards[2]->id()==c3->id());
}

TEST_CASE("Set player's cards", "[player][card]") {

    QVector<Card*> cards;
    cards.push_back(c1);
    cards.push_back(c2);
    cards.push_back(c3);
    p->setCards(cards);
    auto pCards = p->cards();

    REQUIRE(pCards[0]->id()==c1->id());
    REQUIRE(pCards[1]->id()==c2->id());
    REQUIRE(pCards[2]->id()==c3->id());
}

TEST_CASE("Set initial number of tanks for player", "[player]") {

    int initialTanksNum = 21;
    p->setNumOfTanks(initialTanksNum);
    int ret = p->getNumOfTanks();
    REQUIRE(ret==21);
}

TEST_CASE("Get tanks", "[player]") {

    p->addCard(c1);
    p->addCard(c2);
    p->addCard(c3);

    SECTION("3 INFANTRY cards") {
        c1->setCardType(CardType::INFANTRY);
        c2->setCardType(CardType::INFANTRY);
        c3->setCardType(CardType::INFANTRY);
        int expectedValue = 4;

        int returnValue = p->getTanks(c1,c2,c3);

        REQUIRE(expectedValue==returnValue);
    }

    SECTION("3 CAVALRY cards") {
        c1->setCardType(CardType::CAVALRY);
        c2->setCardType(CardType::CAVALRY);
        c3->setCardType(CardType::CAVALRY);
        int expectedValue = 6;

        int returnValue = p->getTanks(c1,c2,c3);

        REQUIRE(expectedValue==returnValue);
    }

    SECTION("3 CANNON cards") {
        c1->setCardType(CardType::CANNON);
        c2->setCardType(CardType::CANNON);
        c3->setCardType(CardType::CANNON);
        int expectedValue = 8;

        int returnValue = p->getTanks(c1,c2,c3);

        REQUIRE(expectedValue==returnValue);
    }

    SECTION("3 cards of different types") {
        c1->setCardType(CardType::INFANTRY);
        c2->setCardType(CardType::CAVALRY);
        c3->setCardType(CardType::CANNON);
        int expectedValue = 10;

        int returnValue = p->getTanks(c1,c2,c3);

        REQUIRE(expectedValue==returnValue);
    }

}

TEST_CASE("Move tanks", "[player][territory]") {

    p->addTerritory(t1);
    p->addTerritory(t2);
    p->addTerritory(t3);

    t1->setNumOfTanks(10);
    t2->setNumOfTanks(6);

    int expectedValueT1 = 7;
    int expectedValueT2 = 9;

    p->moveTanks(t1,t2,3);

    int t1NumOfTanks = t1->numOfTanks();
    int t2NumOfTanks = t2->numOfTanks();

    REQUIRE(expectedValueT1==t1NumOfTanks);
    REQUIRE(expectedValueT2==t2NumOfTanks);
}

TEST_CASE("toString method for player", "[player]") {

    Player* redPlayer = new Player("redPlayer", Color::RED);
    Player* greenPlayer = new Player("greenPlayer", Color::GREEN);
    Player* bluePlayer = new Player("bluePlayer", Color::BLUE);
    Player* yellowPlayer = new Player("yellowPlayer", Color::YELLOW);
    Player* pruplePlayer = new Player("pruplePlayer", Color::PURPLE);
    QString s;

    SECTION("Red player") {
        s = redPlayer->toString();
        REQUIRE(s=="RED");
    }

    SECTION("Green player") {
        s = greenPlayer->toString();
        REQUIRE(s=="GREEN");
    }

    SECTION("Blue player") {
        s = bluePlayer->toString();
        REQUIRE(s=="BLUE");
    }

    SECTION("Yellow player") {
        s = yellowPlayer->toString();
        REQUIRE(s=="YELLOW");
    }

    SECTION("Purple player") {
        s = pruplePlayer->toString();
        REQUIRE(s=="PURPLE");
    }
}

TEST_CASE("Add task card for player", "[player][cardtask]") {

    SECTION("Destroy player card") {
        auto dpcard = new DestroyPlayerCard();
        p->addCardTask(dpcard);
        REQUIRE(p->cardTask()==dpcard);
    }

    SECTION("Region occupy card") {
        auto rocard = new RegionOccupyCard();
        p->addCardTask(rocard);
        REQUIRE(p->cardTask()==rocard);
    }

    SECTION("Territory occupy card") {
        auto tocard = new TerritoryOccupyCard();
        p->addCardTask(tocard);
        REQUIRE(p->cardTask()==tocard);
    }
}
