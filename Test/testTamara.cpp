#include "catch.hpp"

#include "../riziko/card.h"
#include "../riziko/cardtask.h"
#include "../riziko/destroyplayercard.h"
#include "../riziko/game.h"
#include "../riziko/initializer.h"
#include "../riziko/player.h"
#include "../riziko/region.h"
#include "../riziko/regionoccupycard.h"
#include "../riziko/territory.h"
#include "../riziko/territoryoccupycard.h"

TEST_CASE("CHECK TASK - DESTROY PLAYER, TERRITORY OCCUPY, REGION OCCUPY", "player territory region")
{

    Player *player1 = new Player("Tamara", Color::RED);
    Player *player2 = new Player("Maja", Color::BLUE);
    Player *player3 = new Player("Uros", Color::YELLOW);
    Player *player4 = new Player("Ognjen", Color::GREEN);
    Player *player5 = new Player("Nebojsa", Color::PURPLE);

    Game *game = new Game();

    QVector<Player*> players = {player1, player2, player3, player4, player5};

    game->setPlayers(players);

    Initializer *i = new Initializer();
    auto t = i->initializerTerritory(players);
    QVector<Region*> regions = i->initializeRegions();

    QVector<Region*> regions1{regions[0], regions[1]};
    QVector<Region*> regions2{regions[4], regions[1]};
    QVector<Region*> regions3{regions[2], regions[3]};
    QVector<Region*> regions4{regions[2], regions[5]};
    QVector<Region*> regions5{regions[3], regions[5]};
    QVector<Region*> regions6{regions[0], regions[3]};
    QVector<Region*> regions7{regions[0], regions[2]};
    QVector<Region*> regions8{regions[4]};
    QVector<Region*> regions9{regions[4], regions[3]};
    QVector<Region*> regions10{regions[4], regions[5]};
    QVector<Region*> regions11{regions[4], regions[5]};
    QVector<Region*> regions12{regions[4], regions[2]};

    QVector<Region*> allR = {regions[0], regions[1], regions[2], regions[3], regions[4], regions[5]};

    CardTask *cardTask1 = new DestroyPlayerCard("../images/card_task/15.png", game->players()[1]);
    CardTask *cardTask2 = new TerritoryOccupyCard("../images/card_task/13.png", 16);
    CardTask *cardTask3 = new RegionOccupyCard("../images/card_task/10.png", regions10, allR, 2);
    CardTask *cardTask4 = new RegionOccupyCard("../images/card_task/6.png", regions6, allR, 3);
    CardTask *cardTask5 = new DestroyPlayerCard("../images/card_task/19.png", game->players()[4]);

    SECTION("DESTROY PLAYER CARD - TRUE")
    {
        player1->addCardTask(cardTask1);
        player1->setPlayerToDestroy(player2);
        player2->removeAllTerritories();

        bool shouldBe = true;

        bool returnValue = player1->cardTask()->checkTask(player1);

        REQUIRE(shouldBe == returnValue);
    }

    SECTION("DESTROY PLAYER CARD - FALSE")
    {
        player1->addCardTask(cardTask1);
        player1->setPlayerToDestroy(player2);

        bool shouldBe = false;

        bool returnValue = player1->cardTask()->checkTask(player1);

        REQUIRE(shouldBe == returnValue);
    }

    SECTION("TERRITORY OCCUPY CARD (16) - FALSE") {

        player2->addCardTask(cardTask2);

        bool shouldBe = false;

        bool returnValue = player2->cardTask()->checkTask(player2);

        REQUIRE(shouldBe == returnValue);
    }

    SECTION("TERRITORY OCCUPY CARD (16) - TRUE") {

        player2->addCardTask(cardTask2);
        for(auto t : player1->territories()) {
            player2->addTerritory(t);
            t->setOwner(player2);
            //player1->removeTerritory(t);
        }

        int i = 0;
        for(auto t : player3->territories()) {
            if(i != 2) {
                player2->addTerritory(t);
                t->setOwner(player2);
                i++;
            }
            //player3->removeTerritory(t);
        }

        bool shouldBe = true;

        bool returnValue = player2->cardTask()->checkTask(player2);

        REQUIRE(shouldBe == returnValue);
    }

    SECTION("REGION OCCUPY CARD (OCCUPY SCANDINAVIA AND EASTERN EUROPE) - TRUE")
    {
        player3->addCardTask(cardTask3);

        for(auto t : regions[4]->territories()) {
            player3->addTerritory(t);
        }

        bool shouldBe = false;

        bool returnValue = player3->cardTask()->checkTask(player3);

        REQUIRE(shouldBe == returnValue);
    }

    SECTION("REGION OCCUPY CARD (OCCUPY SCANDINAVIA AND EASTERN EUROPE) - TRUE")
    {
        player3->addCardTask(cardTask3);

        for(auto t : regions[4]->territories()) {
            player3->addTerritory(t);
        }

        for(auto t : regions[5]->territories()) {
            player3->addTerritory(t);
        }

        bool shouldBe = true;

        bool returnValue = player3->cardTask()->checkTask(player3);

        REQUIRE(shouldBe == returnValue);

    }

    SECTION("REGION OCCUPY CARD (OCCUPY BALTIC, WESTERN EUROPE AND REGION OF YOUR CHOICE) - TRUE")
    {
        player4->addCardTask(cardTask4);

        for(auto t : regions[0]->territories()) {
            player4->addTerritory(t);
        }

        for(auto t : regions[3]->territories()) {
            player4->addTerritory(t);
        }

        for(auto t : regions[2]->territories()) {
            player4->addTerritory(t);
        }

        bool shouldBe = true;

        bool returnValue = player4->cardTask()->checkTask(player4);

        REQUIRE(shouldBe == returnValue);
    }


    SECTION("REGION OCCUPY CARD (OCCUPY BALTIC, WESTERN EUROPE AND REGION OF YOUR CHOICE) - FALSE")
    {
        player5->addCardTask(cardTask5);
        player5->setPlayerToDestroy(player5);

        for(auto t : player1->territories()) {
            player5->addTerritory(t);
            t->setOwner(player5);
            //player1->removeTerritory(t);
        }

        bool shouldBe = false;

        bool returnValue = player5->cardTask()->checkTask(player5);

        REQUIRE(shouldBe == returnValue);

    }

    SECTION("DESTROY PLAYER CARD BUT YOU ARE THAT PLAYER - TRUE") {
        player5->addCardTask(cardTask5);
        player5->setPlayerToDestroy(player5);

        for(auto t : player1->territories()) {
            player5->addTerritory(t);
            t->setOwner(player5);
            //player1->removeTerritory(t);
        }

        for(auto t : player3->territories()) {
            player5->addTerritory(t);
            t->setOwner(player5);
            //player3->removeTerritory(t);
        }

        int i = 0;
        for(auto t : player4->territories()) {
            if(i < 1) {
                player5->addTerritory(t);
                t->setOwner(player5);
                i++;
            }
            //player3->removeTerritory(t);
        }

        bool shouldBe = true;

        bool returnValue = player5->cardTask()->checkTask(player5);

        REQUIRE(shouldBe == returnValue);
    }

    SECTION("DESTROY PLAYER CARD BUT YOU ARE THAT PLAYER - FALSE") {
        player5->addCardTask(cardTask5);
        player5->setPlayerToDestroy(player5);

        for(auto t : player1->territories()) {
            player5->addTerritory(t);
            t->setOwner(player5);
            //player1->removeTerritory(t);
        }

        int i = 0;
        for(auto t : player4->territories()) {
            if(i < 1) {
                player5->addTerritory(t);
                t->setOwner(player5);
                i++;
            }
            //player3->removeTerritory(t);
        }

        bool shouldBe = false;

        bool returnValue = player5->cardTask()->checkTask(player5);

        REQUIRE(shouldBe == returnValue);
    }


}

