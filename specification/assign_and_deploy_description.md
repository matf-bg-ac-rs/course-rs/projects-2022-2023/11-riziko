# Dodeljivanje i rasporedjivanje armija

**Kratak opis**: Svaki put kada igrac bude na potezu, on dobija tenkove koje treba da rasporedi po svojim teritorijama. Prilikom prvog poteza u partiji, igracu se na pocetku partije dodeljuje 28 tenkova. Igrac treba da rasporedi 21 tenk po svojim teritorijama, jer je preostalih 7 automatski rasporedjeno (na svaku teritoriju igraca po 1). Svaki sledeci put kada bude njegov potez, igrac dobija dodatne tenkove (u zavisnosti od toga koliko teritorija poseduje u tom trenutku) i rasporedjuje ih. Pored toga, na kraju svakog poteza u kom je osvojio najmanje jednu teritoriju, igrac ima pravo da vuce karticu. Kada sakupi 3 kartice moze ih zameniti za dodatne tenkove i njih rasporediti. 

**Akteri**:
Igrac koji je na potezu.

**Preduslovi**: Aplikacija je aktivna i partija je u toku.

**Postuslovi**: 1) U slucaju standardne dodele tenkova na pocetku poteza: Igrac je rasporedio svoje tenkove i spreman je da izvrsi napad.
		2) U slucaju razmene kartica za tenkove : Igrac je rasporedio svoje tenkove i moze da preda potez narednom igracu.

**Osnovni tok**:
0. Prvi put u partiji kada je igrac na potezu (nulti potez), dodeljuje mu se 28 tenkova da ih rasporedi. 
   Prilikom prvog poteza nema dodele dodatnih tenkova, vec se za napad koriste oni dodeljeni i rasporedjeni u nultom potezu. 
1. Svaki sledeci put kada je igrac na potezu, on dobija dodatne tenkove na osnovu broja teritorija koje u tom trenutku poseduje. 
	* 1.1. Broj tenkova koji se dodeljuju igracu se odredjuje po sledecoj formuli: broj teritorija koje igrac u tom trenutku poseduje se podeli sa 3 i zatim zaokruzi na manju celobrojnu vrednost.
	* 1.2. Ukoliko se igrac zatim odluci da napada i tom prilikom osvoji teritoriju, on moze prebaciti dodatne tenkove sa teritorija koje poseduje na novoosvojenu teritoriju (prilikom osvajanja automatski se na novoosvojenu teritoriju prebacuje 1 tenk sa teritorije sa iz koje je izvrsen napad)
	* 1.3. Na kraju svakog poteza igrac moze zameniti kartice za dodatne tenkove.
		* 1.3.1. Za dobijanje dodatnih tenkova potrebne su 3 kartice
			* 1.3.1.1. Broj tenkova koji se dobijaju zavisi od kartica koje igrac menja za njih, razmena se vrsi u skladu sa tabelom prilozenom u dodatnim informacijama
		* 1.3.2. Nakon zamene kartica za tenkove, igrac novodobijene tenkove rasporedjuje po svojim teritorijama. 

**Alternativni tokovi**: /

**Podtokovi**: /

**Specijalni zahtevi**: Igrac na potezu mora biti povezan na internet i posedovati kod sebe klijent aplikacije.
                        Server aplikacije mora biti pokrenut u trenutku pokretanja partije.
                        Igrac mora biti u partiji.

**Dodatne informacije**:


Tabela kartica:

| Kartice                 	| Broj tenkova 	|
|-------------------------	|:------------:	|
| 3 pesadinca             	|       4      	|
| 3 konjanika             	|       6      	|
| 3 topa                  	|       8      	|
| 3 razlicite kartice     	|      10      	|
