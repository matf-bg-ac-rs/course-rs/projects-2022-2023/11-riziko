# Povezivanje igraca na server

**Kratak opis**: Pri pokretanju aplikacije, klijenti salju TCP zahtev za povezivanje na server i nakon uspesnog povezivanja pokrece se glavni meni.

**Akteri**: Klijenti.

**Preduslovi**: /

**Postuslovi**:Igrac je povezan na server i otvoren je glavni meni.

**Osnovni tok**:
1. Pri pokretanju aplikacije klijent salje serveru TCP zahtev za povezivanje.
2. Server odobrava taj zahtev.
3. Uspostavlja se TCP konekcija i otvara se glavni meni.


**Alternativni tokovi**: <br />
A1: Prikazuje se prozor sa porukom o neuspesnom povezivanju i dugmetom za ponovni pokusaj uspostavljanja konekcije.

**Podtokovi**: /

**Specijalni zahtevi**: <br />
Svaki od igraca mora biti povezan na internet i posedovati klijent aplikaciju. Server aplikacije mora biti pokrenut u trenutku zahtevanja konekcije.

**Dodatne informacije**: /


